#!/bin/sh


configure_repositories () {
	local code=0
	local keyfile='var/db/xbps/keys/2e:cd:12:9f:a9:b8:fe:b3:36:ae:d2:cf:56:47:8d:ce.plist'
	local keyfile_in_container="/var/lib/lxc/${NAME}/rootfs/${keyfile}"

        repo_variant= ; [ "$VARIANT" = 'musl' ] && repo_variant='musl'

	lxc-attach --clear-env --name "${NAME}" -- sh -c 'mkdir -p /etc/xbps.d &&
                echo "repository=http://lysator7eknrfl47rlyxvgeamrv7ucefgrrlhk7rouv3sna25asetwid.onion/pub/voidlinux/current/'$repo_variant'" > /etc/xbps.d/00-repository-main.conf &&
		echo "repository=https://splitlinux.gitlab.io/split-packages" > /etc/xbps.d/10-repository-splitlinux.conf &&
		touch "/var/db/xbps/keys/2e:cd:12:9f:a9:b8:fe:b3:36:ae:d2:cf:56:47:8d:ce.plist"' || code=1

	[ -s "/${keyfile}" -a -f "${keyfile_in_container}" ] && cat "/${keyfile}" > "${keyfile_in_container}" || code=2

	return $code
}


install_packages () {
	local code=0

	# NOTE beast-skel is added so that the user created by this scripts starts
	#      out with a properly prepared .xinitrc (and nice environment defaults).
	#      The package is installed in a separate command to avoid a clash with
	#      'xinit' which also writes /etc/skel/.xinitrc.
	lxc-attach --clear-env --name "${NAME}" -- sh -c 'SOCKS_PROXY="socks5://172.18.0.2:9050" xbps-install -Syu' &&
		lxc-attach --clear-env --name "${NAME}" -- sh -c 'SOCKS_PROXY="socks5://172.18.0.2:9050" xbps-install -y xbps' &&
		lxc-attach --clear-env --name "${NAME}" -- sh -c 'SOCKS_PROXY="socks5://172.18.0.2:9050" xbps-install -y dwm st torsocks xorg-fonts xorg-minimal zsh' &&
		lxc-attach --clear-env --name "${NAME}" -- sh -c 'SOCKS_PROXY="socks5://172.18.0.2:9050" xbps-install -y beast-skel' ||
		code=1

	if [ "${VARIANT}" = 'default' ]; then
		lxc-attach --clear-env --name "${NAME}" -- sh -c 'xbps-reconfigure -f glibc-locales' || code=2
	fi

	return $code
}

set_dns_resolution () {
        tor_ip=$(sed --silent 's|^lxc\.net\.0\.ipv4\.address = \(.*\)\..*/16|\1.2|p' "/var/lib/lxc/${NAME}/config")

        if [ "$tor_ip" ] ; then
	        lxc-attach --clear-env --name "${NAME}" -- sed -i 's|^|# |g' /etc/resolv.conf
	        lxc-attach --clear-env --name "${NAME}" -- sh -c "printf '\n# Split Linux torified DNS\noptions use-vc\nnameserver %s\n' '$tor_ip' >> /etc/resolv.conf"
        fi
}

set_user_shell () {
        lxc-attach --clear-env --name "${NAME}" -- usermod --shell /bin/zsh "${NAME}"
}


display_final_hints () {
        last_getty=$(find /var/service/ -maxdepth 1 -regex '.*\/agetty-tty[0-9]+' | sort | tail -n1 | sed 's|^\/var\/service\/agetty-tty||g')

        # TODO DRY with splt-configure-archlinux 
	echo
	echo '———'
	echo 'The container was created with a very minimal default graphical interface.'
	echo "Feel free to now install Beast, Split Linux' desktop environment for fast workflows:"
	echo
	echo "  lxc-attach ${NAME} -- sh -c 'xbps-remove -y dwm && SOCKS_PROXY=socks5://172.18.0.2:9050 xbps-install -Sy beast'"
	echo
	echo 'Also note that the container is created as "isolated" for maximum anonymity.'
	echo 'If you want to switch to "leaky" or "exposed" you may do so with the "splt" command:'
	echo
	echo "  splt route ${NAME} leaky"
	echo 'OR'
	echo "  splt route ${NAME} exposed"
	echo
	echo "If you're happy with the results just login as user ${NAME} with the password you chose."
	echo "Remember that Ctrl-Alt-F1 through -F${last_getty} bring you to any of the corresponding virtual terminals."
	echo '———'
}


readonly NAME=$1 && shift
readonly VARIANT=$1 && shift

if lxc-attach --clear-env --name "${NAME}" -- grep -q '^ID="void"$' /etc/os-release ; then
        echo 'Configuring container ...'

        configure_repositories && install_packages && set_dns_resolution &&
                set_user_shell && display_final_hints
fi
