# Template file for 'beast-dwm'
pkgname=beast-dwm
version=6.3
revision=1
wrksrc=dwm-${version}
makedepends="libXinerama-devel libXft-devel freetype-devel"
depends="beast-dwm-yellow beast-scripts"
short_desc="Beast dynamic window manager for X"
maintainer="Kevin Crumb <kevcrumb@splitlinux.org>"
license="MIT"
homepage="https://splitlinux.org/"
distfiles="https://dl.suckless.org/dwm/dwm-${version}.tar.gz"
checksum=badaa028529b1fba1fd7f9a84f3b64f31190466c858011b53e2f7b70c6a3078d
conflicts="dwm"

_COLOR_VARIANTS="blue-0000ff cyan-00ffff green-00ff00 grey-424242 magenta-ff00ff red-ff0000 white-ffffff yellow-ffff00"

do_patch() {
	# Hotkeys may also be defined via keycodes (physical position independant of keyboard layout).
	patch -i ${FILESDIR}/dwm-6.2-keycodes.patch

	# The mouse cursor position shall not determine which window is active.
	patch -i ${FILESDIR}/dwm-6.2-mouseresistance.patch

        # Remove border when there is only one window visible.
        patch -i ${FILESDIR}/dwm-6.2-noborderselflickerfix.patch

	# bb3bd6fe in dwm 6.2 breaks commands that bring up windows.
	# With that commit in place "wmctrl -xa" and "xdotool windowactivate" fail.
	patch -i ${FILESDIR}/dwm-6.2-undo_bb3bd6fe.patch
}

do_build() {
	[ -e ${FILESDIR}/config.h ] && cp ${FILESDIR}/config.h config.h

	sed -i "/CFLAGS/s|\${CPPFLAGS}|& $CFLAGS|g" config.mk
	sed -i "/LDFLAGS/s|\-s|$LDFLAGS|g" config.mk

	for c in $_COLOR_VARIANTS ; do
		color="${c%%-*}"
		code="${c##*-}"
		sed -i 's/^\(static const char col_primary\[\] *=\).*/\1 "'#$code'";/g' config.h
		make CC=$CC INCS="-I. -I${XBPS_CROSS_BASE}/usr/include/freetype2" LIBS="-lX11 -lXinerama -lXft -lfontconfig"
		cp -a dwm dwm-$color
	done
}

do_install() {
	make PREFIX=/usr DESTDIR=$DESTDIR install
        # To always install all color-variants
	#for c in $_COLOR_VARIANTS ; do vbin "dwm-${c%%-*}" ; done

	vinstall README 644 usr/share/doc/$pkgname
	vinstall ${FILESDIR}/dwm.desktop 644 usr/share/xsessions
	vlicense LICENSE

	vdoc ${FILESDIR}/layout1_alt.png
	vdoc ${FILESDIR}/layout2_super.png
	vdoc ${FILESDIR}/layout3_super_alt.png
}



beast-dwm-blue_package() {
	short_desc+=" - blue"
	alternatives="dwm:/usr/bin/dwm:/usr/bin/dwm-blue"
	pkg_install() { vbin dwm-blue ; }
}

beast-dwm-cyan_package() {
	short_desc+=" - cyan"
	alternatives="dwm:/usr/bin/dwm:/usr/bin/dwm-cyan"
	pkg_install() { vbin dwm-cyan ; }
}

beast-dwm-green_package() {
	short_desc+=" - green"
	alternatives="dwm:/usr/bin/dwm:/usr/bin/dwm-green"
	pkg_install() { vbin dwm-green ; }
}

beast-dwm-grey_package() {
	short_desc+=" - grey"
	alternatives="dwm:/usr/bin/dwm:/usr/bin/dwm-grey"
	pkg_install() { vbin dwm-grey ; }
}

beast-dwm-magenta_package() {
	short_desc+=" - magenta"
	alternatives="dwm:/usr/bin/dwm:/usr/bin/dwm-magenta"
	pkg_install() { vbin dwm-magenta ; }
}

beast-dwm-red_package() {
	short_desc+=" - red"
	alternatives="dwm:/usr/bin/dwm:/usr/bin/dwm-red"
	pkg_install() { vbin dwm-red ; }
}

beast-dwm-white_package() {
	short_desc+=" - white"
	alternatives="dwm:/usr/bin/dwm:/usr/bin/dwm-white"
	pkg_install() { vbin dwm-white ; }
}

beast-dwm-yellow_package() {
	short_desc+=" - yellow"
	alternatives="dwm:/usr/bin/dwm:/usr/bin/dwm-yellow"
	pkg_install() { vbin dwm-yellow ; }
}
