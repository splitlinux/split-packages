/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=10" };
static const char dmenufont[]       = "monospace:size=10";
static const char col_primary[]     = "#ffff00";
static const char col_secondary[]   = "#000000";
static const char col_gray1[]       = "#222222";
//static const char col_gray2[]       = "#444444";
//static const char col_gray3[]       = "#bbbbbb";
//static const char col_gray4[]       = "#eeeeee";
//static const char col_cyan[]        = "#005577";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_primary, col_secondary, col_gray1 },
	[SchemeSel]  = { col_secondary, col_primary, col_primary },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */

        // 1 << 8: 1 (one) shifted to the left by eight positions generates mask 10000000,
        // thus selecting tag ‘9’ (ninth from the right) in the the tags array.
        // Details: http://dwm.suckless.org/customisation/tagmask
	// NOTE: Apparently the instance name has to be used in the "class" column here.
	// Use  xprop | awk '/WM_CLASS/{ print $4 }'  to find the string to provide.
	/* class          instance    title       tags mask     isfloating   monitor */
	{ "Alacritty",    NULL,       NULL,       0,            0,           -1 },
	{ "URxvt",        NULL,       NULL,       0,            0,           -1 },
     // { "Deluge",       NULL,       NULL,       1 << 1,       0,           -1 },
     // { "Shotcut",      NULL,       NULL,       1 << 1,       0,           -1 },
     // { "google-chrome",NULL,       NULL,       1 << 2,       0,           -1 },
     // { "chrome",       NULL,       NULL,       1 << 2,       0,           -1 },
     // { "Chromium",     NULL,       NULL,       1 << 2,       0,           -1 },
     // { "Firefox",      NULL,       NULL,       1 << 2,       0,           -1 },
     // { "Tor Browser",  NULL,       NULL,       1 << 2,       0,           -1 },
     // { "Keepassx",     NULL,       NULL,       1 << 2,       0,           -1 },
     // { "Sylpheed",     NULL,       NULL,       1 << 3,       0,           -1 },
        { "Gimp",         NULL,       NULL,       1 << 7,       0,           -1 },
     // { "Conky",        NULL,       NULL,       1 << 8,       1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.618; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       NULL, KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           NULL, KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             NULL, KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, NULL, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-fn", dmenufont, "-nb", col_secondary, "-nf", col_primary, "-sb", col_primary, "-sf", col_secondary, NULL };
static const char *termcmd[]  = { "alacritty", NULL };
static const char *lock_screen[]  = { "sh", "-c", "BDlock_screen || i3lock || slock", NULL };
static const char *lock_screen_and_mine[]  = { "sh", "-c", "BDmine_monero & (BDlock_screen locked_and_mining & wait $! && while pgrep --uid $USER --exact i3lock ; do sleep 1 ; done && pkill -9 --exact csminer)", NULL };
//sh -c 'BDlock_screen & wait $! && while pgrep --uid $USER --exact i3lock ; do echo not killing miner yet ; sleep 1 ; done && echo killing miner now'

static const char *lock_and_suspend_to_ram[]  = { "sh", "-c", "BDlock_screen && echo deep | sudo tee /sys/power/mem_sleep && echo mem | sudo tee /sys/power/state", NULL };
static const char *paste_snippet[]  = { "sh", "-c", "BDpaste_snippet", NULL };

// Put this in /etc/sudoers file: /usr/bin/nohup /bin/sh -c touch /run/nologin && sleep 5 && poweroff
static const char *poweroff[]  = { "sh", "-c", "sudo /usr/bin/nohup /bin/sh -c 'touch /run/nologin && sleep 5 && poweroff' & (sleep 1 && wait `pidof touch` && killall --user ${USER})", NULL };

// Put this in /etc/sudoers file: /usr/bin/nohup /bin/sh -c touch /run/nologin && sleep 5 && reboot
static const char *reboot[]  = { "sh", "-c", "sudo /usr/bin/nohup /bin/sh -c 'touch /run/nologin && sleep 5 && reboot' &> /dev/null & (sleep 1 && wait `pidof touch` && killall --user ${USER})", NULL };

/* Hotkeys - set XDG_CONFIG_HOME in xinitrc */
// TODO move these two into hotkey scripts
//static const char *start_xev[]  = { "st", "-f", "monaco:size=19", "-t", "xev" , "-e", "xev", NULL };
//static const char *start_xzoom[]  = { "xzoom", NULL };
static const char *execute_f1_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F1.sh",  NULL };
static const char *execute_f2_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F2.sh",  NULL };
static const char *execute_f3_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F3.sh",  NULL };
static const char *execute_f4_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F4.sh",  NULL };
static const char *execute_f5_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F5.sh",  NULL };
static const char *execute_f6_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F6.sh",  NULL };
static const char *execute_f7_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F7.sh",  NULL };
static const char *execute_f8_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F8.sh",  NULL };
static const char *execute_f9_action[]   = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F9.sh",  NULL };
static const char *execute_f10_action[]  = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F10.sh", NULL };
static const char *execute_f11_action[]  = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F11.sh", NULL };
static const char *execute_f12_action[]  = { "sh", "-c", "\"${XDG_CONFIG_HOME:=$HOME/.config}\"/beast/hotkeys/F12.sh", NULL };


/* Audio (mpd) - set MPD_REMOTE_HOST in xinitrc */
static const char *remote_mpd_next[]             = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" next", NULL };
static const char *remote_mpd_prev[]             = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" prev", NULL };
static const char *remote_mpd_stop[]             = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" stop", NULL };
static const char *remote_mpd_seek_back10[]      = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" seek ' -10'", NULL };
static const char *remote_mpd_seek_back30[]      = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" seek ' -30'", NULL };
static const char *remote_mpd_seek_forward20[]   = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" seek +20", NULL };
static const char *remote_mpd_seek_forward30[]   = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" seek +30", NULL };
static const char *remote_mpd_toggle[]           = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" toggle", NULL };
static const char *remote_mpd_decrease_volume[]  = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" volume -5", NULL };
static const char *remote_mpd_increase_volume[]  = { "sh", "-c", "mpc --host \"${MPD_REMOTE_HOST:=localhost}\" volume +5", NULL };

/* Audio - if MPD_HOST is set (e.g. in xinitrc), mpc will use it */
static const char *mpv_or_mpd_next[]             = { "sh", "-c", "pidof mpv && (jo command=`jo -a -- playlist-next` | socat - /tmp/mpvsocket) || mpc next", NULL };
static const char *mpv_or_mpd_prev[]             = { "sh", "-c", "pidof mpv && (jo command=`jo -a -- playlist-prev` | socat - /tmp/mpvsocket) || mpc prev", NULL };
static const char *mpv_or_mpd_stop[]             = { "sh", "-c", "pidof mpv && (jo command=`jo -a -- stop` | socat - /tmp/mpvsocket) || mpc stop", NULL };
static const char *mpv_or_mpd_seek_back10[]      = { "sh", "-c", "pidof mpv && (jo command=`jo -a -- seek ' -10'` | socat - /tmp/mpvsocket) || mpc seek -10", NULL };
static const char *mpv_or_mpd_seek_back30[]      = { "sh", "-c", "pidof mpv && (jo command=`jo -a -- seek ' -30'` | socat - /tmp/mpvsocket) || mpc seek -30", NULL };
static const char *mpv_or_mpd_seek_forward20[]   = { "sh", "-c", "pidof mpv && (jo command=`jo -a -- seek 20` | socat - /tmp/mpvsocket) || mpc seek +20", NULL };
static const char *mpv_or_mpd_seek_forward30[]   = { "sh", "-c", "pidof mpv && (jo command=`jo -a -- seek 30` | socat - /tmp/mpvsocket) || mpc seek +30", NULL };
static const char *mpd_show_current_id3[]        = { "sh", "-c", "BAshow_id3", NULL };
static const char *mpd_play_track[]              = { "sh", "-c", "BAplay_track", NULL };
static const char *mpv_or_mpd_toggle[]           = { "sh", "-c", "pidof mpv && (jo command=`jo -a keypress space` | socat - /tmp/mpvsocket) || mpc toggle", NULL };
static const char *decrease_volume[]             = { "sh", "-c", "amixer set Master,0 5%- unmute", NULL };
static const char *increase_volume[]             = { "sh", "-c", "amixer set Master,0 5%+ unmute", NULL };
// With some devices sound is only heard if all three of 'Master', 'Headphone' and 'Speaker' are set to unmute.
// Conversely only 'Master' has to be muted to silence everything.
// So volume_mute always ensures that 'Headphone' and 'Speaker' are unmuted while 'Master' is actually toggled on/off.
static const char *volume_mute[]                 = { "sh", "-c", "amixer set Master toggle && for c in Headphone Speaker; do amixer set $c unmute; done", NULL };

// TODO When backlight brightness is at maximum add xrandr-based brightness for bad lighting conditions.
// Concept:
// outputs=`xrandr -q | grep --word-regexp connected | grep -E '[0-9]x[0-9]' | awk '{ print $1 }' 2>/dev/null`
// echo $outputs| while read line ; do xrandr --output $line --brightness 1 ; done
static const char *increase_brightness[]  = { "zsh", "-c", "expr `cat /sys/class/backlight/*/brightness` + 87 | sudo tee /sys/class/backlight/*/brightness", NULL };
static const char *decrease_brightness[]  = { "zsh", "-c", "expr `cat /sys/class/backlight/*/brightness` - 87 | sudo tee /sys/class/backlight/*/brightness", NULL };
static const char *increase_opacity[]     = { "sh", "-c", "transset --actual --inc 0.05 --max 1.00", NULL };
static const char *decrease_opacity[]     = { "sh", "-c", "transset --actual --dec 0.05 --min 0.10", NULL };
static const char *toggle_fullscreen[]    = { "sh", "-c", "xdotool windowstate --toggle FULLSCREEN \"`xdotool getactivewindow`\"", NULL };

/* Jump to or start applications */
// Consider adding "xdotool search --sync --limit=1 --classname <XYZ> windowactivate --sync" _after_ starting new apps - in case they're configured to open on a different tag than the current one.
static const char *jump_start_browser[]      = { "sh", "-c",
	"xdotool search --limit=1 --classname 'Navigator|Tor|Launcher|chromium|firefox|google-chrome' windowactivate --sync ||"
	"( BDlaunch_browser || firefox --private-window || chromium --incognito || google-chrome --incognito ) &", NULL };
//static const char *jump_start_cantata[]      = { "sh", "-c", "xdotool search --limit=1 --classname cantata windowactivate --sync || cantata", NULL };
//static const char *jump_start_chrome[]       = { "sh", "-c", "xdotool search --limit=1 --classname chromium windowactivate --sync || xdotool search --limit=1 --classname google-chrome windowactivate --sync || chromium --incognito || google-chrome --incognito", NULL };
//static const char *jump_start_conky[]        = { "sh", "-c", "xdotool search --limit=1 --classname conky windowactivate --sync || conky -D", NULL };
//static const char *jump_start_deluge[]       = { "sh", "-c", "xdotool search --limit=1 --classname deluge windowactivate --sync || deluge", NULL };
//static const char *jump_start_firefox[]      = { "sh", "-c", "xdotool search --limit=1 --classname firefox windowactivate --sync || firefox", NULL };
//static const char *jump_start_gmpc[]         = { "sh", "-c", "xdotool search --limit=1 --classname gmpc windowactivate --sync || gmpc", NULL };
//static const char *jump_start_keepassx[]     = { "sh", "-c", "xdotool search --limit=1 --classname keepassx windowactivate --sync || keepassx", NULL };
//static const char *jump_start_libreoffice[]  = { "sh", "-c", "xdotool search --limit=1 --classname libreoffice windowactivate --sync || libreoffice", NULL };
static const char *jump_start_mpv[]          = { "sh", "-c",
	"xdotool search --limit=1 --classname mpv windowactivate --sync ||"
	" cd \"`xdg-user-dir MPV`\" && find ./ -type f -maxdepth 1 | sort --random-sort --random-source=/dev/urandom | mpv --playlist=-", NULL };
//static const char *jump_start_mupdf[]        = { "sh", "-c", "xdotool search --limit=1 --classname mupdf windowactivate --sync || mupdf", NULL };
static const char *jump_start_shotcut[]      = { "sh", "-c", "xdotool search --limit=1 --classname shotcut windowactivate --sync || shotcut", NULL };
static const char *jump_start_qutebrowser[]  = { "sh", "-c", "xdotool search --limit=1 --classname qutebrowser windowactivate --sync || qutebrowser || xdotool search --limit=1 --classname surf windowactivate --sync || surf", NULL };
static const char *jump_start_sxiv[]         = { "sh", "-c", "xdotool search --limit=1 --classname sxiv    windowactivate --sync || BDbrowse_images \"`xdg-user-dir SXIV`\"", NULL };
static const char *jump_start_sylpheed[]     = { "sh", "-c", "xdotool search --limit=1 --classname sylpheed windowactivate --sync || sylpheed", NULL };
//static const char *jump_start_thunderbird[]  = { "sh", "-c", "xdotool search --limit=1 --classname thunderbird windowactivate --sync || thunderbird", NULL };
//static const char *jump_start_wireshark[]    = { "sh", "-c", "xdotool search --limit=1 --classname wireshark windowactivate --sync || wireshark", NULL };

// A previous concept had key combinations to jump to an application, but not start it if it was not already running. This had limited utility, but could be re-enabled if use cases arise.
//static const char *jump_cantata[]      = { "sh", "-c", "xdotool search --limit=1 --classname cantata windowactivate --sync", NULL };

static const char *play_selection_in_espeak[]  = { "sh", "-c", "pkill --exact espeak || xsel -po | fold -sw 51 | espeak -v ${BEAST_VOICE:-en-us}", NULL };
// For some reason using named key 'Insert' fails with "st", while its keycode 118 does work.
static const char *start_clipmenu_and_paste[]          = { "sh", "-c", "clipmenu -i -nb black -nf yellow -sb yellow -sf black && xdotool key --clearmodifiers 'Shift_L+118'", NULL };
static const char *paste_and_clear_selections[]        = { "sh", "-c", "BDmanage_passwords autologin", NULL };
static const char *show_qr_code_of_selection[]         = { "sh", "-c", "xsel -po | qrencode --size=20 --output=- | feh --image-bg black --scale-down --auto-zoom --fullscreen -", NULL };
static const char *show_selection_in_sent[]            = { "sh", "-c", "xsel -po | fold -sw 51 | sent -", NULL };
//static const char *start_keynav[]                      = { "sh", "-c", "keynav", NULL };
static const char *start_xkill[]                       = { "sh", "-c", "xkill", NULL };
static const char *logout[]                            = { "sh", "-c", "pkill -u \"$USER\"", NULL };

static const char *tmux_copy_mode[]           = { "sh", "-c", "tmux copy-mode && tmux show-buffer | xsel -ib", NULL };
static const char *tmux_paste_buffer[]        = { "tmux", "paste-buffer", NULL };
static const char *tmux_next_window[]         = { "tmux", "next-window", NULL };
static const char *tmux_previous_window[]     = { "tmux", "previous-window", NULL };
static const char *tmux_split_vertically[]    = { "sh", "-c", "tmux split-window -v -c '#{pane_current_path}' &&"
	"xdotool search --limit=1 --classname alacritty windowactivate --sync ||"
	"xdotool search --limit=1 --classname urxvt windowactivate --sync", NULL };
static const char *tmux_split_horizontally[]  = { "sh", "-c", "tmux split-window -h -c '#{pane_current_path}' && xdotool search --limit=1 --classname alacritty windowactivate --sync || xdotool search --limit=1 --classname urxvt windowactivate --sync",  NULL };
//static const char *tmux_select_1[]            = { "sh", "-c", "tmux select-window -Tt :1", NULL };
//static const char *tmux_select_2[]            = { "sh", "-c", "tmux select-window -Tt :2", NULL };
//static const char *tmux_select_3[]            = { "sh", "-c", "tmux select-window -Tt :3", NULL };
//static const char *tmux_select_4[]            = { "sh", "-c", "tmux select-window -Tt :4", NULL };
//static const char *tmux_select_5[]            = { "sh", "-c", "tmux select-window -Tt :5", NULL };
static const char *tmux_toggle_one[]          = { "sh", "-c", "BDtoggle_window 1", NULL };
static const char *tmux_toggle_four[]         = { "sh", "-c", "BDtoggle_window 4", NULL };
static const char *tmux_toggle_two[]          = { "sh", "-c", "BDtoggle_window 2", NULL };
static const char *tmux_toggle_nine[]         = { "sh", "-c", "BDtoggle_window 9", NULL };
static const char *tmux_toggle_six[]          = { "sh", "-c", "BDtoggle_window 6", NULL };
static const char *tmux_toggle_five[]         = { "sh", "-c", "BDtoggle_window 5", NULL };
static const char *tmux_toggle_eight[]        = { "sh", "-c", "BDtoggle_window 8", NULL };
static const char *tmux_toggle_three[]        = { "sh", "-c", "BDtoggle_window 3", NULL };
static const char *tmux_toggle_seven[]        = { "sh", "-c", "BDtoggle_window 7", NULL };

/* Handy helpers */
// These click events currently don't work
static const char *mouse_click_left[]              = { "sh", "-c", "xdotool click 1", NULL };
static const char *mouse_click_middle[]            = { "sh", "-c", "xdotool click 2", NULL };
static const char *mouse_click_right[]             = { "xdotool click 3", NULL };
// Copy & paste previous tmux line
static const char *tmux_paste_previous_line[]      = { "sh", "-c", "tmux copy-mode && sleep 0.5 && (xdotool key Up & wait $!) && (xdotool key 0 key 0 key space & wait $!) && (xdotool type $ & wait $!) && (xdotool type $ & wait $!) && (xdotool key h key Return & wait $!) && tmux paste-buffer", NULL };
static const char *tmux_paste_last_word_of_previous_line[]  = { "sh", "-c", "tmux copy-mode && sleep 0.5 && (xdotool key Up & wait $!) && (xdotool type $ & wait $!) && (xdotool type $ & wait $!) && (xdotool key B key space & wait $!) && (xdotool type $ & wait $!) && (xdotool key h key Return & wait $!) && tmux paste-buffer", NULL };
static const char *open_current_wallpaper[]        = { "sh", "-c", "BDbrowse_images \"`cat /tmp/BDrandomize_wallpaper.${USER}`\"", NULL };
// TODO implement writing -previous file
static const char *revert_to_previous_wallpaper[]  = { "sh", "-c", "feh --bg-fill \"`cat /tmp/BDrandomize_wallpaper.${USER}-previous`\"", NULL };
static const char *log_instant[]                   = { "sh", "-c", "BDlog_instant", NULL };
static const char *randomize_wallpaper[]           = { "sh", "-c", "BDrandomize_wallpaper", NULL };
static const char *randomize_wallpaper1[]          = { "sh", "-c", "BDrandomize_wallpaper \"`xdg-user-dir WALLPAPER1`\" recursive", NULL };
static const char *randomize_wallpaper2[]          = { "sh", "-c", "BDrandomize_wallpaper \"`xdg-user-dir WALLPAPER2`\" recursive", NULL };

/* Screenshots */ 
static const char *screenshot_active_window[]      = { "sh", "-c", "BDtake_screenshot active_window", NULL };
static const char *screenshot_fullscreen[]         = { "sh", "-c", "BDtake_screenshot fullscreen", NULL };
static const char *screenshot_cursor_selection[]   = { "sh", "-c", "BDtake_screenshot cursor_selection", NULL };

static const Key keys[] = {
	/* modifier                      keycode   keysym     function        argument */

	/* Keycodes instead of keysyms or characters are used here because the position of the physical keys independent of any keyboard layout is what is relevant.
	 * Use `xev | grep keycode` to find them. As a comment the corresponding key char on a qwerty keyboard is given. */

        /* Audio & Remote Audio */
	{ Mod1Mask|Mod4Mask,                 31,   NULL,       spawn,         {.v = increase_volume } },            // i
        { Mod1Mask|Mod4Mask|ControlMask,     31,   NULL,       spawn,         {.v = remote_mpd_increase_volume } }, // i
	{ Mod1Mask|Mod4Mask,                 30,   NULL,       spawn,         {.v = decrease_volume } },            // u
        { Mod1Mask|Mod4Mask|ControlMask,     30,   NULL,       spawn,         {.v = remote_mpd_decrease_volume } }, // u

	{ Mod1Mask|Mod4Mask,                 44,   NULL,       spawn,         {.v = mpv_or_mpd_toggle } },          // j
        { Mod1Mask|Mod4Mask|ControlMask,     44,   NULL,       spawn,         {.v = remote_mpd_toggle } },          // j
	{ Mod1Mask|Mod4Mask,                 33,   NULL,       spawn,         {.v = mpv_or_mpd_stop } },            // p
        { Mod1Mask|Mod4Mask|ControlMask,     33,   NULL,       spawn,         {.v = remote_mpd_stop } },            // p
	{ Mod1Mask|Mod4Mask,                 41,   NULL,       spawn,         {.v = mpd_play_track } },             // f
	{ Mod1Mask|Mod4Mask,                 43,   NULL,       spawn,         {.v = mpv_or_mpd_prev } },
        { Mod1Mask|Mod4Mask|ControlMask,     43,   NULL,       spawn,         {.v = remote_mpd_prev } },
        // TODO add shift-variants for more precise seeking
	{ Mod1Mask|Mod4Mask,                 45,   NULL,       spawn,         {.v = mpv_or_mpd_seek_back30 } },     // k
        { Mod1Mask|Mod4Mask|ControlMask,     45,   NULL,       spawn,         {.v = remote_mpd_seek_back30 } },     // k
	{ Mod1Mask|Mod4Mask,                 46,   NULL,       spawn,         {.v = mpv_or_mpd_seek_forward30 } },  // l
        { Mod1Mask|Mod4Mask|ControlMask,     46,   NULL,       spawn,         {.v = remote_mpd_seek_forward30 } },  // l
        { Mod1Mask|Mod4Mask,                 59,   NULL,       spawn,         {.v = mpv_or_mpd_seek_back10 } },
        { Mod1Mask|Mod4Mask|ControlMask,     59,   NULL,       spawn,         {.v = remote_mpd_seek_back10 } },
        { Mod1Mask|Mod4Mask,                 60,   NULL,       spawn,         {.v = mpv_or_mpd_seek_forward20 } },
        { Mod1Mask|Mod4Mask|ControlMask,     60,   NULL,       spawn,         {.v = remote_mpd_seek_forward20 } },
	{ Mod1Mask|Mod4Mask,                 47,   NULL,       spawn,         {.v = mpv_or_mpd_next } },
        { Mod1Mask|Mod4Mask|ControlMask,     47,   NULL,       spawn,         {.v = remote_mpd_next } },
	{ Mod1Mask|Mod4Mask,                 58,   NULL,       spawn,         {.v = volume_mute } },                // m
	{ Mod1Mask|Mod4Mask,               NULL,   XK_Left,    spawn,         {.v = mpv_or_mpd_prev } },
        { Mod1Mask|Mod4Mask|ControlMask,   NULL,   XK_Left,    spawn,         {.v = remote_mpd_prev } },
	{ Mod1Mask|Mod4Mask,               NULL,   XK_Right,   spawn,         {.v = mpv_or_mpd_next } },
        { Mod1Mask|Mod4Mask|ControlMask,   NULL,   XK_Right,   spawn,         {.v = remote_mpd_next } },
	{ Mod1Mask|Mod4Mask,               NULL,   XK_Return,  spawn,         {.v = mpd_show_current_id3 } },

        { 0,                               NULL,   0x1008ff12, spawn,         {.v = volume_mute } },                // XF86AudioMute
        { 0,                               NULL,   0x1008ff13, spawn,         {.v = increase_volume } },
        { 0,                               NULL,   0x1008ff11, spawn,         {.v = decrease_volume } },

        { 0,                               NULL,   0x1008ff02, spawn,         {.v = increase_brightness } },        // XF86MonBrightnessUp
        { 0,                               NULL,   0x1008ff03, spawn,         {.v = decrease_brightness } },        // XF86MonBrightnessDown

        /* Rii mini remote */
	{ 0,                               NULL,   0x1008ff17, spawn,         {.v = mpv_or_mpd_next } },            // XF86AudioNext
	{ 0,                               NULL,   0x1008ff14, spawn,         {.v = mpv_or_mpd_toggle } },          // XF86AudioPlay
	{ 0,                               NULL,   0x1008ff16, spawn,         {.v = mpv_or_mpd_prev } },            // XF86AudioPrev
        { 0,                               NULL,   0x1008ff19, spawn,         {.v = tmux_toggle_nine } },           // XF86Mail
        { 0,                               NULL,   0x1008ff1b, spawn,         {.v = tmux_toggle_two } },            // XF86Search
        { 0,                               NULL,   0x1008ff18, spawn,         {.v = tmux_toggle_seven } },          // XF86HomePage

        /* Jump to or start Applications */
	{ Mod4Mask,                          38,   NULL,       spawn,         {.v = tmux_toggle_one } },            // a
	{ Mod4Mask,                          39,   NULL,       spawn,         {.v = tmux_toggle_two } },            // s
	{ Mod4Mask,                          40,   NULL,       spawn,         {.v = tmux_toggle_three } },          // d
	{ Mod4Mask,                          41,   NULL,       spawn,         {.v = tmux_toggle_four } },           // f
	{ Mod4Mask,                          42,   NULL,       spawn,         {.v = tmux_toggle_five } },           // g
	{ Mod4Mask,                          43,   NULL,       spawn,         {.v = tmux_toggle_six } },            // h
	{ Mod4Mask,                          44,   NULL,       spawn,         {.v = tmux_toggle_seven } },          // j
	{ Mod4Mask,                          45,   NULL,       spawn,         {.v = tmux_toggle_eight } },          // k
	{ Mod4Mask,                          46,   NULL,       spawn,         {.v = tmux_toggle_nine } },           // l
	{ Mod4Mask,                          47,   NULL,       spawn,         {.v = jump_start_qutebrowser } },     // :

        /* Handy helpers */
	// { Mod1Mask,                          52,   NULL,       spawn,         {.v = mouse_click_left } },              // z
	// { Mod1Mask,                          53,   NULL,       spawn,         {.v = mouse_click_middle } },            // x
	// { Mod1Mask,                          54,   NULL,       spawn,         {.v = mouse_click_right } },             // c
        { Mod1Mask,                          38,   NULL,       spawn,         {.v = play_selection_in_espeak } },         // a say_selected
        { Mod1Mask,                          44,   NULL,       spawn,         {.v = paste_snippet } },                    // j paste_snippet
        { Mod1Mask,                          40,   NULL,       spawn,         {.v = show_qr_code_of_selection } },        // d qrcode_selected
        { Mod1Mask,                          45,   NULL,       spawn,         {.v = start_clipmenu_and_paste } },         // k paste_clipmenu
        { Mod1Mask,                          43,   NULL,       spawn,         {.v = paste_and_clear_selections } },       // h typing_paste
	{ Mod1Mask,                          46,   NULL,       spawn,         {.v = tmux_paste_last_word_of_previous_line } }, // l repeat_tmux_word
        { Mod1Mask,                          39,   NULL,       spawn,         {.v = show_selection_in_sent } },           // s present_selected
        { Mod1Mask,                          47,   NULL,       spawn,         {.v = tmux_paste_previous_line } },         // ; repeat_tmux_line
	{ Mod1Mask,                          48,   NULL,       spawn,         {.v = tmux_split_horizontally } },          // ' split_tm_horiz
	{ Mod1Mask,                          61,   NULL,       spawn,         {.v = tmux_split_vertically } },            // / split_tm_verti
	{ Mod1Mask,                          59,   NULL,       spawn,         {.v = tmux_previous_window } },             // , prev_tm_window
	{ Mod1Mask,                          60,   NULL,       spawn,         {.v = tmux_next_window } },                 // . next_tm_window
	{ Mod1Mask,                          56,   NULL,       spawn,         {.v = tmux_copy_mode } },                   // b tm_copy_mode
	{ Mod1Mask,                          57,   NULL,       spawn,         {.v = tmux_paste_buffer } },                // n paste_tm_buffer

       	{ Mod4Mask,                          57,   NULL,       spawn,         {.v = jump_start_browser } },
     //	{ Mod4Mask|ControlMask,              57,   NULL,       spawn,         {.v = jump_start_chrome } },          // n
     //	{ Mod4Mask,                          27,   NULL,       spawn,         {.v = jump_start_conky } },           // r
     // { Mod4Mask,                          32,   NULL,       spawn,         {.v = jump_feh } },                   // o
     // { Mod4Mask|ControlMask,              32,   NULL,       spawn,         {.v = jump_start_feh } },             // o
     // { Mod4Mask,                          32,   NULL,       spawn,         {.v = jump_deluge } },                // o
     // { Mod4Mask|ControlMask,              32,   NULL,       spawn,         {.v = jump_start_deluge } },          // o
     // { Mod4Mask,                          32,   NULL,       spawn,         {.v = jump_start_firefox } },         // o
     //	{ Mod4Mask|ControlMask,              32,   NULL,       spawn,         {.v = jump_start_firefox } },         // o
     // { Mod4Mask,                          31,   NULL,       spawn,         {.v = jump_start_gmpc } },            // i
     // { Mod4Mask,                          31,   NULL,       spawn,         {.v = jump_cantata } },               // i
     //	{ Mod4Mask|ControlMask,              31,   NULL,       spawn,         {.v = jump_start_cantata } },         // i
     //	{ Mod4Mask|ControlMask,              30,   NULL,       spawn,         {.v = jump_start_shotcut } },         // u
     // { Mod4Mask,                          29,   NULL,       spawn,         {.v = jump_start_keepassx } },        // y
     // { Mod4Mask,                          26,   NULL,       spawn,         {.v = jump_start_libreoffice } },     // e
	{ Mod4Mask,                        NULL,   XK_Return,  spawn,         {.v = jump_start_mpv } },
     //	{ Mod4Mask|ControlMask,              58,   NULL,       spawn,         {.v = start_mpv } },                  // m
     //	{ Mod4Mask,                          55,   NULL,       spawn,         {.v = jump_start_mupdf } },           // v
        { Mod4Mask,                          51,   NULL,       spawn,         {.v = jump_start_sxiv } },
     // { Mod4Mask|ControlMask,              33,   NULL,       spawn,         {.v = jump_start_sxiv } },
     //	{ Mod4Mask,                          25,   NULL,       spawn,         {.v = jump_start_virtualbox } },      // w
     //	{ Mod4Mask,                          28,   NULL,       spawn,         {.v = jump_start_wireshark } },       // t
	{ Mod4Mask,                          48,   NULL,       spawn,         {.v = jump_start_sylpheed } },        // '
	{ Mod4Mask,                          59,   NULL,       spawn,         {.v = tmux_previous_window } },       // , prev_tm_window
	{ Mod4Mask,                          60,   NULL,       spawn,         {.v = tmux_next_window } },           // . next_tm_window

     // { Mod4Mask|ControlMask,            NULL,   XK_space,   spawn,         {.v = start_keynav } },

        /* Screenshots */
  	{ 0,                               NULL,   XK_Print,   spawn,         {.v = screenshot_active_window } },
  	{ ShiftMask,                       NULL,   XK_Print,   spawn,         {.v = screenshot_fullscreen } },
  	{ Mod1Mask,                        NULL,   XK_Print,   spawn,         {.v = screenshot_cursor_selection } },

     //	{ Mod1Mask,                        NULL,   XK_Left,    spawn,         {.v = decrease_volume } },
     //	{ Mod1Mask,                        NULL,   XK_Right,   spawn,         {.v = increase_volume } },
	{ Mod1Mask,                        NULL,   XK_Delete,  spawn,         {.v = start_xkill } },
       	{ Mod1Mask,                        NULL,   XK_F1,      spawn,         {.v = execute_f1_action } },
       	{ Mod1Mask,                        NULL,   XK_F2,      spawn,         {.v = execute_f2_action } },
	{ Mod1Mask,                        NULL,   XK_F3,      spawn,         {.v = execute_f3_action } },
	{ Mod1Mask,                        NULL,   XK_F4,      spawn,         {.v = execute_f4_action } },
	{ Mod1Mask,                        NULL,   XK_F5,      spawn,         {.v = execute_f5_action } },
	{ Mod1Mask,                        NULL,   XK_F6,      spawn,         {.v = execute_f6_action } },
	{ Mod1Mask,                        NULL,   XK_F7,      spawn,         {.v = execute_f7_action } },
	{ Mod1Mask,                        NULL,   XK_F8,      spawn,         {.v = execute_f8_action } },
	{ Mod1Mask,                        NULL,   XK_F9,      spawn,         {.v = execute_f9_action } },
	{ Mod1Mask,                        NULL,   XK_F10,     spawn,         {.v = execute_f10_action } },
	{ 0,                               NULL,   XK_F11,     spawn,         {.v = toggle_fullscreen } },
	{ Mod1Mask,                        NULL,   XK_F11,     spawn,         {.v = execute_f11_action } },
	{ Mod1Mask,                        NULL,   XK_F12,     spawn,         {.v = execute_f12_action } },

        /* Window opacity/transparency */
        { Mod1Mask,                        NULL,   XK_Prior,   spawn,         {.v = decrease_opacity } },
        { Mod1Mask,                        NULL,   XK_Next,    spawn,         {.v = increase_opacity } },

        { 0,                               NULL,   0x1008ff2a, spawn,         {.v = poweroff } },                   // XF86PowerOff
	{ Mod1Mask,                        NULL,   XK_End,     spawn,         {.v = logout } },
	{ Mod1Mask|ShiftMask,              NULL,   XK_End,     spawn,         {.v = reboot } },
	{ Mod1Mask,                        NULL,   XK_Home,    spawn,         {.v = poweroff } },
        { 0,                               NULL,   XK_Pause,   spawn,         {.v = lock_screen } },
        { ShiftMask,                       NULL,   XK_Pause,   spawn,         {.v = lock_screen_and_mine } },
	{ Mod1Mask,                        NULL,   XK_Pause,   spawn,         {.v = lock_and_suspend_to_ram } },
        { Mod1Mask,                        NULL,   XK_Menu,    spawn,         {.v = log_instant } },

        /* dwm default bindings */
	//{ MODKEY,                        NULL,   XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                            41,   NULL,      spawn,          {.v = dmenucmd } },      // f run_command
	{ MODKEY|ShiftMask,                NULL,   XK_Return, spawn,          {.v = termcmd } },
	//{ MODKEY,                        NULL,   XK_b,      togglebar,      {0} },
	{ MODKEY,                            55,   NULL,      togglebar,      {0} },                   // v toggle_statusbar
	//{ MODKEY,                        NULL,   XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                            31,   NULL,      focusstack,     {.i = +1 } },            // i next_window
	//{ MODKEY,                        NULL,   XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                            30,   NULL,      focusstack,     {.i = -1 } },            // u prev_window
	//{ MODKEY,                        NULL,   XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                            24,   NULL,      incnmaster,     {.i = +1 } },            // q more_windows
	//{ MODKEY,                        NULL,   XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                            25,   NULL,      incnmaster,     {.i = -1 } },            // w less_windows
	//{ MODKEY,                        NULL,   XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                            26,   NULL,      setmfact,       {.f = -0.05} },          // e less_space
	//{ MODKEY,                        NULL,   XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                            27,   NULL,      setmfact,       {.f = +0.05} },          // r more_space
	{ MODKEY,                          NULL,   XK_Return, zoom,           {0} },                   // Return  change_masterwindow
	{ MODKEY,                          NULL,   XK_Tab,    view,           {0} },                   // Tab     toggle_view
	//{ MODKEY,                        NULL,   XK_c,      killclient,     {0} },
	{ MODKEY,                            42,   NULL,      killclient,     {0} },                   // g kill_window
	//{ MODKEY,                        NULL,   XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                            53,   NULL,      setlayout,      {.v = &layouts[0]} },    // x tile_layout
	//{ MODKEY,                        NULL,   XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                            94,   NULL,      setlayout,      {.v = &layouts[1]} },    // < float_layout
	{ MODKEY,                            52,   NULL,      setlayout,      {.v = &layouts[4]} },    // z dwindle_layout
	//{ MODKEY,                        NULL,   XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                            54,   NULL,      setlayout,      {.v = &layouts[2]} },    // c monocle_layout
	{ MODKEY,                          NULL,   XK_space,  setlayout,      {0} },                   // Space   toggle_layout
	{ MODKEY|ShiftMask,                NULL,   XK_space,  togglefloating, {0} },
	{ MODKEY,                          NULL,   XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,                NULL,   XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                            32,   NULL,      focusmon,       {.i = -1 } },            // o prev_monitor
	{ MODKEY,                            33,   NULL,      focusmon,       {.i = +1 } },            // p next_monitor
	{ MODKEY|ShiftMask,                NULL,   XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,                NULL,   XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                                   XK_1,                      0)
	TAGKEYS(                                   XK_2,                      1)
	TAGKEYS(                                   XK_3,                      2)
	TAGKEYS(                                   XK_4,                      3)
	TAGKEYS(                                   XK_5,                      4)
	TAGKEYS(                                   XK_6,                      5)
	TAGKEYS(                                   XK_7,                      6)
	TAGKEYS(                                   XK_8,                      7)
	TAGKEYS(                                   XK_9,                      8)
	{ MODKEY|ShiftMask,                NULL,   XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */

        /* Wallpapers */
        { ClkRootWin,           Mod1Mask,          Button1,     spawn,          {.v = randomize_wallpaper } },
        { ClkRootWin,           Mod1Mask,          Button2,     spawn,          {.v = randomize_wallpaper2 } },
        // NOTE The actually desired Button9 is not declared/available:
        { ClkRootWin,           Mod1Mask,          Button3,     spawn,          {.v = open_current_wallpaper } },
        { ClkRootWin,           Mod1Mask,          Button4,     spawn,          {.v = randomize_wallpaper1 } },
        { ClkRootWin,           Mod1Mask,          Button5,     spawn,          {.v = revert_to_previous_wallpaper } },

        /* Audio */
        { ClkLtSymbol,          Mod1Mask|Mod4Mask, Button4,     spawn,          {.v = increase_volume } },
        { ClkStatusText,        Mod1Mask|Mod4Mask, Button4,     spawn,          {.v = increase_volume } },
        { ClkWinTitle,          Mod1Mask|Mod4Mask, Button4,     spawn,          {.v = increase_volume } },
        { ClkClientWin,         Mod1Mask|Mod4Mask, Button4,     spawn,          {.v = increase_volume } },
        { ClkRootWin,           Mod1Mask|Mod4Mask, Button4,     spawn,          {.v = increase_volume } },
        { ClkTagBar,            Mod1Mask|Mod4Mask, Button4,     spawn,          {.v = increase_volume } },

        { ClkLtSymbol,          Mod1Mask|Mod4Mask, Button5,     spawn,          {.v = decrease_volume } },
        { ClkStatusText,        Mod1Mask|Mod4Mask, Button5,     spawn,          {.v = decrease_volume } },
        { ClkWinTitle,          Mod1Mask|Mod4Mask, Button5,     spawn,          {.v = decrease_volume } },
        { ClkClientWin,         Mod1Mask|Mod4Mask, Button5,     spawn,          {.v = decrease_volume } },
        { ClkRootWin,           Mod1Mask|Mod4Mask, Button5,     spawn,          {.v = decrease_volume } },
        { ClkTagBar,            Mod1Mask|Mod4Mask, Button5,     spawn,          {.v = decrease_volume } },

	/* Toggle todo.textile */
        { ClkStatusText,        0,              Button1,        spawn,          {.v = tmux_toggle_eight } },

	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

