#!/bin/sh

# Ensure this script is called at startup (for example via ~/.xinitrc).
#
# It creates indices of your files and directories to allow for quick searches
# and navigation.
#
# Activate the "incron" service to keep the indices up-to-date as you add and
# delete contents. This script creates a default incron table configured to
# keep track of your home directory.
#
# You can fine-tune through lines like these via the command `incrontab -e`:
#
# /path/to/indexed_place1 IN_CREATE,IN_DELETE,IN_MOVED_FROM,IN_MOVED_TO  /usr/bin/timeout 5  <absolute_path_to_this_script> $@/$# $%
# /path/to/indexed_place2 IN_CREATE,IN_DELETE,IN_MOVED_FROM,IN_MOVED_TO  /usr/bin/timeout 5  <absolute_path_to_this_script> $@/$# $%


should_be_ignored () {
        local hidden_file='^\..*'

        # zsh continously creates and deletes .zsh_history.LOCK
        # Already matched by "hidden_file", this pattern is named for emphasis:
        local hidden_lockfile='^\..*\.LOCK'

        local vim_tempfile='.*~$'

        local ignore_pattern="${hidden_file}|${hidden_lockfile}|${vim_tempfile}"
        basename "$1" | grep --quiet --extended-regexp "${ignore_pattern}"
}

wait_for_completion_of_other_instances () {
        readonly local WAIT_INTERVAL=.1

        while [ -e "${LOCKFILE}" ]; do
		echo "Script already running (locked by $LOCKFILE). Attempting again in ${WAIT_INTERVAL}."
                sleep ${WAIT_INTERVAL}
        done
}

check_for_incrond () {
        pgrep --exact incrond > /dev/null ||
                echo 'WARNING: incrond service is not running. Activate it to keep the index up to date automatically.'
}

initialize_incrontab () {
        local home_dir=`xdg-user-dir || echo $HOME`
        local incron_line="$home_dir   IN_CREATE,IN_DELETE,IN_MOVED_FROM,IN_MOVED_TO  /usr/bin/timeout 5  BDupdate_index \$@/\$# \$%"

        local tmp_script=`mktemp --tmpdir delayed_tee-XXX.sh`
        echo '#!/bin/sh\nsleep 1 && tee "$1"' > "$tmp_script"
        chmod +x "$tmp_script"

        echo "$incron_line" | EDITOR="$tmp_script" incrontab -e
        rm "$tmp_script"
}

add_directory () {
	local ABSOLUTE_DIRECTORY="${1}" && shift
	local directory=`echo "${ABSOLUTE_DIRECTORY}" | cut -d '/' -f 4-`
        local new_directories=''

        # Index directories recursively up from the file's location.
        while echo "$directory" | grep --quiet --fixed-strings '/' ; do
	        grep --quiet "^${directory}$" "$DIRECTORY_INDEX" ||
                        new_directories="$directory\n${new_directories}"
                directory=${directory%/*}
        done
	grep --quiet "^${directory}$" "$DIRECTORY_INDEX" ||
                new_directories="$directory ${new_directories}"

        printf "${new_directories}" >> "$DIRECTORY_INDEX"
}

update_index () {
        # If you have more than one fzopen place, it's suggested to explicitly monitor
        # these directories in incrontab, as opposed to just monitoring one main parent
        # directory.
        #
        # Override the following by naming the places you want to monitor in the
        # environment variable FZOPEN_PLACES, 
        [ -z "$FZOPEN_PLACES" ] && FZOPEN_PLACES="${HOME}/*"

        case "${OPERATION}" in
                IN_CREATE|IN_MOVED_TO)
                        # NOTE: As of a test on 2022-03-26, incron mostly fails to execute this for new
                        #       directories. Thus this section will typically only be reached when a file,
			#       not a directory, is created.
                        local index="$FILE_INDEX"
                        [ "${TYPE}" = 'IN_ISDIR' ] && index="$DIRECTORY_INDEX"

                        PLACE_CUT_AND_ESCAPED="`echo ${PLACE} | cut -d '/' -f 4- | sed 's:[]\[^$.*/]:\\\&:g'`"

                        # Ignore duplicates
                        if ! grep --quiet --extended-regexp "^${PLACE_CUT_AND_ESCAPED}$" "${index}" ; then
                                # Grepping for $PLACE_PATTERN ensures that the file is only added if $PLACE is in $FZOPEN_PLACES
                                PLACE_PATTERN=`echo "${FZOPEN_PLACES}" | sed 's#^#^#g ; s# #|^#g'`
                                echo "${PLACE}" | grep -E "${PLACE_PATTERN}" | cut -d '/' -f 4- >> "${index}"

                                # To partly work around the above-mentioned issue of ignored directory-creation,
                                # this indexes new directories once files are added to them:
                                [ -f "${PLACE}" ] && add_directory "${PLACE%/*}"
                        fi
                        ;;
                IN_DELETE|IN_MOVED_FROM)
                        local index="$FILE_INDEX"
                        [ "${TYPE}" = 'IN_ISDIR' ] && index="$DIRECTORY_INDEX"

                        # A deletion immediately followed by a creation (as caused by Vim, for example) is
                        # sometimes recorded by incrond in reverse order.
                        # Verify that PLACE is actually gone to handle the condition:
                        if [ ! -e "${PLACE}" ] ; then
                                PLACE_CUT_AND_ESCAPED="`echo ${PLACE} | cut -d '/' -f 4- | sed 's:[]\[^$.*/]:\\\&:g'`"
                                sed -i "/^${PLACE_CUT_AND_ESCAPED}$/d" "${index}"
                        fi
                        ;;
                *) # 'UPDATE_DB')
                        check_for_incrond
                        find ${FZOPEN_PLACES} -type f ! -name '*~' ! -path '*/_site/*' ! -path '*/.*' | cut -d '/' -f 4- | sort -r > "${FILE_INDEX}"
                        find ${FZOPEN_PLACES} -type d ! -name '*~' ! -path '*/_site/*' ! -path '*/.*' | cut -d '/' -f 4- | sort > "${DIRECTORY_INDEX}"
                        [ -e "/var/spool/incron/$USER" ] || initialize_incrontab
                        ;;
        esac
}


# When incrond calls this script, it passes the place that changed as $1 and a
# list like "IN_DELETE,IN_ISDIR" as $2.
readonly PLACE="$1"
readonly OPERATION="`echo $2 | cut -f1 -d','`"
readonly TYPE="`echo $2 | cut -f2 -d','`"
# echo $PLACE $2 >> /tmp/log

readonly FILE_INDEX=/tmp/.file_index
readonly DIRECTORY_INDEX=/tmp/.directory_index
readonly LOCKFILE="${TEMP:-/tmp}/.`basename $0`.lock"

if ! should_be_ignored "$PLACE" ; then
        wait_for_completion_of_other_instances

	touch "${LOCKFILE}"

        update_index

	rm -f "${LOCKFILE}"
fi
