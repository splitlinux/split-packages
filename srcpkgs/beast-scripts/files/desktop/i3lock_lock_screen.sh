#!/bin/sh

image_filename=${1:-locked}

readonly DISPLAY_GEOMETRY=$(xdotool getdisplaygeometry | sed 's# #x#g')

image_dir="${XDG_CONFIG_HOME:-~/.config}/beast/lockscreens"
svg="${image_dir}/${image_filename}.svg"
png="${image_dir}/${DISPLAY_GEOMETRY}-${image_filename}.png"

if [ -e "$png" ] ; then
	echo "Using existing $png ..."
else
	if [ -e "$svg" ] ; then
		echo "Creating $png ..."
		rsvg-convert --width ${DISPLAY_GEOMETRY%x*} \
			--height ${DISPLAY_GEOMETRY#*x} \
			"${svg}" --output "${png}"
	else
		echo "No SVG source image found at $svg"
	fi
fi


# Locking fails when the mouse is hidden/inactive/grabbed by a program like
# `unclutter`. The first command frees the mouse by moving the cursor.
xdotool mousemove 0 0 && xdotool mousemove --polar 0 0 &&
        i3lock --show-failed-attempts --beep --tiling --image "${png}" || i3lock
