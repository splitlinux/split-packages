#!/bin/sh

#network=`ip -brief -family inet address |
#		sed --silent 's#.*\(172\..*\..*\)\/.*$#\1#p'`
#torproxy_guess="${network%.*}.2"

# Apparantly `torsocks` doesn't work for Go applications.
#torsocks -a "${torproxy_guess}" -P 9050 --isolate \

threads="$1"

if [ -z "$threads" ] ; then
        threads="${BEAST_MINER_THREADS:-`nproc`}"

        [ "$threads" -gt 0  2>/dev/null ] || threads=1
fi

if pid=$(pidof csminer) ; then
        echo "csminer already found running (PID $pid). Aborting." >&2
else
        if [ -x "`command -v abduco`" ] ; then
                abduco -fn 'csminer'      csminer --user "${1:-splitlinux-org}" --saver=false --threads "$threads"
        else
        	screen -d -m -S 'csminer' csminer --user "${1:-splitlinux-org}" --saver=false --threads "$threads"
        fi
fi
