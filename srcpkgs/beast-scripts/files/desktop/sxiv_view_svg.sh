#!/bin/sh

readonly IMAGE="${1}"
readonly TMP=`mktemp --tmpdir XXX-${IMAGE##.*/}.png`

rsvg-convert --output "${TMP}" "${IMAGE}" &&
        sxiv "${TMP}" || inkview "${IMAGE}"

rm -f "${TMP}"
