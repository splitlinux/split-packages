#/bin/sh

# Especially useful for keyboard shortcuts

# Passing things like '-1' as OFFSET causes an additional jump relative to target_window.
#
# EXAMPLE: The following call toggles the tmux window left of the one called 'VI'.
# ./tmux_toggle_window.sh 'VI' '-1'

# for debugging
#osd () {
#	echo "$@" | osd_cat --delay 1 -p middle -A center -l2 \
#		-f "-*-clean-*-*-*-*-64-*-*-*-*-*-*-*" -O1 -c "yellow" &
#}

determine_target_window () {
	if [ "${OFFSET}" = '' ]; then
	        target_window="${DESTINATION}"
	else
	        local before_or_after='A' && head_or_tail='tail'
	        expr ${OFFSET} \< 0 > /dev/null && before_or_after='B' && head_or_tail='head'
	
	        local offset=`echo "${OFFSET}" | sed 's#-##g' | sed 's#\+##g'`
	
	        local target_window_info=`tmux list-windows |
			grep -"${before_or_after}" "${offset}" "${DESTINATION}" | ${head_or_tail} -n1`
	
	        # In order to support window names containing spaces and asterisks,
	        # the following sequence is a little complex.
	        # NOTE Window names with trailing hyphens are currently unsupported due
		#      to tmux always appending a hyphen to the last active window.
	        readonly GET_UPTO_END_OF_NAME="sed 's,\s*(.*,,g'"
	        readonly REMOVE_WINDOW_NUMBER='cut -d " " -f 2-'
	        readonly REMOVE_TRAILING_Z="sed 's,Z\$,,g'"
	        readonly REMOVE_TRAILING_EXCLAMATION_MARK="sed 's,\!\$,,g'"
	        readonly REMOVE_TRAILING_ASTERISK="sed 's,*\$,,g'"
	        readonly REMOVE_TRAILING_HYPHEN="sed 's,-\$,,g'"
	        target_window=`echo "${target_window_info}" |
			eval "${GET_UPTO_END_OF_NAME}" |
			eval "${REMOVE_WINDOW_NUMBER}" |
			eval "${REMOVE_TRAILING_Z}" |
			eval "${REMOVE_TRAILING_HYPHEN}" |
			eval "${REMOVE_TRAILING_EXCLAMATION_MARK}"`
	
	        echo target_window: "#${target_window}#"
	        if [ "`echo ${target_window_info} | grep ' (active)$'`" ]; then
	                target_window="`echo ${target_window} | eval ${REMOVE_TRAILING_ASTERISK}`"
	        fi
	fi
}

jump_start_x_window () {
	command='transset -a 0.90 & tmux attach || BDlaunch_console'
	# TODO run automatic window and pane arrangements upon startup
	xdotool search --limit=1 --classname "${BEAST_TERMINAL}" windowactivate --sync ||
		$BEAST_TERMINAL -e sh -c "${command}"
}

jump_to_tmux_window () {
        # echo "${target_window} window currently inactive. Switching to it ..."

        # `find-window` offers a select-menu in case more windows share the same
	# name, while `select-window` appears to simply deny switching in such
	# cases.
	#
	# NOTE Since tmux 2.6-1, `find-window` always brings up selection dialog
	#      even when the looked up window name is unique making it unfit for
	#      the task at hand. In tmux 3.1b this is still the case.
        #tmux find-window -N "${target_window}"
	#
	# `select-window` requires matching the beginning of the window name and
	# is thus less flexible, but it might work for the use cases at hand.
        tmux select-window -T -t "${target_window}"
}

already_on_correct_tmux_window () {
	[ "${target_window}" = "${CURRENT_TMUX_WINDOW}" -o \
                "${target_window}" = "${CURRENT_TMUX_WINDOW_NUMBER}" ]
}

already_on_correct_x_window () {
	#osd "$CURRENT_X_WINDOW"

	# For alacritty "$BEAST_TERMINAL" matches
	# For urxvt "zsh" matches
	echo "${CURRENT_X_WINDOW} = ${BEAST_TERMINAL}"
	[ "${CURRENT_X_WINDOW}" = "${BEAST_TERMINAL}" \
		-o "${CURRENT_X_WINDOW}" = 'tmux' \
		-o "${CURRENT_X_WINDOW}" = 'zsh' ]
}



readonly DESTINATION="$1"
readonly OFFSET=${2:-''}
BEAST_TERMINAL="${BEAST_TERMINAL:-alacritty}"

readonly CURRENT_X_WINDOW="`xdotool getwindowfocus getwindowname |
                            tr '[:upper:]' '[:lower:]'`"

readonly CURRENT_TMUX_WINDOW="`tmux display-message -p '#W'`"
readonly CURRENT_TMUX_WINDOW_NUMBER="`tmux display-message -p '#I'`"


determine_target_window

if already_on_correct_tmux_window; then

        if already_on_correct_x_window; then
                tmux last-window
        fi

else
	jump_to_tmux_window
fi

# `xdotool search` matches the window _after_ the current one, effectively
# rotating through any matches encountered. This condition avoids that.
if ! already_on_correct_x_window; then
	jump_start_x_window
fi


# When the terminal is resized back to full size, the "other-pane" stays wide.
# To tackle the issue this workaround resets the layout for "〔📝 〕" every time
# this script is run.
tmux select-layout -t '〔📝 〕' main-vertical
