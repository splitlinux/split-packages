#!/bin/sh

readonly ACTION="$1" && shift

list_available_entries () {
	readonly BASE_PATH="${HOME}/.password-store"

        find "${BASE_PATH}/" ! -path "${BASE_PATH}/\.*" -a \( -type f -o -type l \) |
                cut -d '/' -f 5- | sed "s/\.gpg$//g"
}

find_match () {
	readonly TMP=`mktemp --tmpdir .passdb-XXX`
        list_available_entries > "${TMP}"

	# TODO make BDfuzzy_find its own package/executable as simply "gzy"
	readonly MATCH=`BDfuzzy_find "${TMP}" "$@"`

	rm "${TMP}"
}

dmenu_find_match () {
        readonly PRE_FILTER="${@:-}"

        available_entries=`list_available_entries | grep --fixed-strings "${PRE_FILTER}"`
        readonly amount_of_available_entries=`echo "${available_entries}" | wc -l`

        [ "${amount_of_available_entries}" -ne 1 ] &&
                available_entries=`echo "${available_entries}" | dmenu \
                        -nb "${BD_DMENU_COLOR1:-black}"  -nf "${BD_DMENU_COLOR2:-yellow}" \
                        -sb "${BD_DMENU_COLOR2:-yellow}" -sf "${BD_DMENU_COLOR1:-black}"`

	readonly MATCH="${available_entries}"
}

match_and_paste_using_dmenu () {
	dmenu_find_match $@

        if [ -z ${MATCH} ] ; then
                echo 'Aborted.'
        else
                populate_selections
                paste_and_clear_selections
        fi
}

populate_selections () {
	pass_show="`pass show $MATCH`"
	echo "$pass_show" | sed -n 's#^username: \(.*\)#\1#p' | xsel --primary
	echo "$pass_show" | head -n1 | xsel --secondary
	#echo "Populated primary selection with username '`xsel --primary`' and secondary selection with the corresponding password."
}

paste_and_clear_selections () {
        local linecount_in_primary=`xsel --primary | wc -l`
        local linecount_in_secondary=`xsel --secondary | wc -l`
        local error=0

        if [ $linecount_in_secondary -eq 1 ] ; then

                if [ $linecount_in_primary -eq 1 ] ; then
                        xsel --primary | (sleep 0.1 && xdotool type --clearmodifiers --file -) &&
                                (sleep 0.1 && xdotool key --clearmodifiers Tab)
                fi

                xsel --secondary | (sleep 0.1 && xdotool type --clearmodifiers --file -) &&
                        xsel --clear --secondary && xsel --clear --primary &&
                        (sleep 1 && xdotool key --clearmodifiers Return)
        else
                error=2
        fi

        return $error
}

case "${ACTION}" in
	find|show|edit|rm)
		find_match $@
		# In case of "find" the $MATCH found by find_match is used directly
		# because pass' "find" would also output any and all symlinks.
		[ "${ACTION}" = 'find' ] && echo "${MATCH}" || pass "${ACTION}" "${MATCH}"
		;;
        autologin)
                paste_and_clear_selections || match_and_paste_using_dmenu
		;;
	add|mv)
		echo "Commands for '${ACTION}' are handed to the 'pass' command without prior modification."
		echo "If you want support for shell completion, use 'pass ${ACTION}' directly."
		pass ${ACTION} $@
		;;
	clip|clippass)
		find_match $@
		populate_selections

		if [ "${ACTION}" = 'clippass' ] ; then
                        xsel --exchange && xsel --secondary --clear
                fi
		;;
	visit)
		find_match $@
		populate_selections
		uri="`echo $MATCH | cut -d '/' -f2- | sed 's#\(.*\)\^.*#\1#g ; s#^#https://#g'`"

		BROWSER=${BROWSER:-tor-browser}
		$BROWSER $uri &
		;;
	*)
		echo "Action '${ACTION}' is unexpected." >&2
		echo
		echo "    pass ${ACTION} $@"
		echo
		echo -n 'Press ENTER to execute as shown.' &&
			read && echo && pass ${ACTION} $@
		;;
esac
