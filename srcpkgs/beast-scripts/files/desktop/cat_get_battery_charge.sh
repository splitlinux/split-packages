#!/bin/sh

BAT=${1:-BAT1}

battery_path="/sys/class/power_supply/${BAT}"

if [ -d "${battery_path}" ] ; then
	CUR=`cat "${battery_path}/energy_now"`
	MAX=`cat "${battery_path}/energy_full"`
	STATUS=`cat "${battery_path}/status"`
	LAST_PERCENTAGE_FILE="/tmp/`basename $0`.last_charge-${USER}"
	THRESHOLD_PERCENTAGE=6
	
	touch "${LAST_PERCENTAGE_FILE}"
	percentage=$((100 * ${CUR} / ${MAX}))
	
	if [ "${STATUS}" != 'Charging' -a "${percentage}" -le ${THRESHOLD_PERCENTAGE} ]; then
	        last_percentage=`cat "${LAST_PERCENTAGE_FILE}"`
		last_percentage=${last_percentage:='0'}
	
		# TODO replace `sent` by `osd_cat` or `osdbattery`
	        [ ${last_percentage} -gt ${percentage} ] &&
			echo "battery charge at     ${percentage} %" | sent &
	
	        echo -n "${percentage}" > "${LAST_PERCENTAGE_FILE}"
	fi
	
	echo "${percentage}"
else
	echo "No battery found at ${battery_path}" >&2
fi
