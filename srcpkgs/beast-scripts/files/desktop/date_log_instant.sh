#!/bin/sh

# To calculate the resulting count, run: cat "${LOG_FILE}" | wc -l
# To reset, just delete the file: rm "${LOG_FILE}"

LOG_FILE="${TMPDIR:-/tmp}/`basename $0`.$USER"
date '+%Y-%m-%d %H:%M:%S %z' >> "${LOG_FILE}"
