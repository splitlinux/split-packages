#!/bin/sh

initialize () {
        DATE=$(date --utc +%Y%m%d_%H%M%S_%Z)

        # As of 2022-03 detox (v1.4.5) appears to be broken on musl
        local detox_cmd=cat
        (ldd `which inline-detox` | grep -qvw ld-musl) && detox_cmd=inline-detox

        TOXIC_CHARACTERS='s#\(\/\|\s\)#_#g'
        WINDOW_NAME=$(xdotool getwindowname `xdotool getactivewindow` | sed "$TOXIC_CHARACTERS" | $detox_cmd | cut -c -200)
}

active_window () {
        initialize
        WINDOW_GEOMETRY=$(xdotool getwindowgeometry `xdotool getactivewindow` | grep Geometry: | awk '{ print $2 }')
        maim --window=$(xdotool getactivewindow) "$HOME/Downloads/${DATE}_${WINDOW_NAME}_${WINDOW_GEOMETRY}.png"
}

fullscreen () {
        initialize
        DISPLAY_GEOMETRY=$(xdotool getdisplaygeometry | sed 's# #x#g')
        maim "$HOME/Downloads/${DATE}_Fullscreen_with_${WINDOW_NAME}_${DISPLAY_GEOMETRY}.png"
}

cursor_selection () {
        initialize
        maim --select "$HOME/Downloads/${DATE}.png"
}

eval "$*"
