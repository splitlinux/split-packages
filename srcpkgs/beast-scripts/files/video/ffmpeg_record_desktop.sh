#!/bin/sh

ffmpeg_record_window () {
        FORMAT="$1"

        DATE=`date +%Y%m%d_%H%M%S`

        XWININFO=`xwininfo`
        # WINDOW_ID=`echo "${XWININFO}" |grep "Window id:"|sed -e "s/xwininfo\:\ Window id:\ // ;s/\ .*//"`
        # WINDOW_ID=`echo "${XWININFO}" | sed --silent 's#.* Window id: \(.*\) ".*"#\1#p'`

        # As of 2022-03 detox (v1.4.5) appears to be broken on musl
        local detox_cmd=cat
        (ldd `which inline-detox` | grep -qvw ld-musl) && detox_cmd=inline-detox

        TOXIC_CHARACTERS='s#\(\/\|\s\)#_#g'
        WINDOW_NAME=`echo "${XWININFO}" | sed --silent 's#.* Window id: .*"\(.*\)"#\1#p' | sed "$TOXIC_CHARACTERS" | $detox_cmd | cut -c -200`

        WINDOW_WIDTH=`echo "${XWININFO}" |  sed --silent 's#^\s*Width: \(.*\)#\1#p'`
        WINDOW_HEIGHT=`echo "${XWININFO}" |  sed --silent 's#^\s*Height: \(.*\)#\1#p'`
        WINDOW_GEOMETRY="${WINDOW_WIDTH}x${WINDOW_HEIGHT}"

        X_OFFSET=`echo "${XWININFO}" | sed --silent 's#^\s*Absolute upper-left X:  \(.*\)#\1#p'`
        Y_OFFSET=`echo "${XWININFO}" | sed --silent 's#^\s*Absolute upper-left Y:  \(.*\)#\1#p'`

        local filename="${DATE}_${WINDOW_NAME}_${WINDOW_GEOMETRY}.${FORMAT:-mp4}"
        ffmpeg -f x11grab -framerate 25 -video_size ${WINDOW_GEOMETRY} \
                -i +${X_OFFSET},${Y_OFFSET} "${filename}"
}

ffmpeg_record_selection () {
        FORMAT="$1"
        SLOPSWITCHES="${2}f"

        ffmpeg -f x11grab -framerate 25 $(slop "-$SLOPSWITCHES" '-video_size %wx%h -i +%x,%y') "desktop_area_recording.${FORMAT:-mp4}"
}

ffmpeg_record_desktop () {
        FORMAT="$1"

        local fullscreen=`xrandr | grep '*' | awk '{ print $1 }'`
        ffmpeg -video_size "${fullscreen}" -framerate 25 -f x11grab -i "${DISPLAY:-:0.0}" -f alsa -ac 2 -i hw:0 "desktop_recording.${FORMAT:-mp4}"
}


record="${1:-window}"
[ -n "$1" ] && shift

case "$record" in
        d|de|des|desk|deskt|desktop)                    ffmpeg_record_desktop   $@ ; break ;;
        s|se|sel|sele|selec|selecti|selectio|selection) ffmpeg_record_selection $@ ; break ;;
        window)                                         ffmpeg_record_window    $@ ; break ;;
esac
