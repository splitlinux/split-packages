#!/bin/sh
set -ue

#TMP=$(mktemp --tmpdir --directory $(basename $0)-XXX)

cd /etc/skel && find . -mindepth 1 -type f ! -name 'todo.textile' -printf '%P\n' -exec sh -c '
        TOOL=sdiff

        processed_files=""
        for filepath in "$@" ; do 
                filepath=${filepath##\.\/}
                mkdir -p "$HOME/$(dirname $filepath)"

                skel="/etc/skel/$filepath"
                against="$HOME/$filepath" 
                backup="$against.$(date +%Y-%m-%d@%H-%M-%S.%N)~"
                [ -s "$skel" ] && if ! diff "$skel" "$against" ; then
                        clear

                        echo "Running $TOOL ..."
                        echo
                        echo "Will merge into the file in $HOME (LEFT) from /etc/skel (RIGHT)."
                        echo "Next file: $filepath"
                        echo
                        echo "Press ENTER" && read key

                        [ ! -f "$against" ] && touch "$against"
                        cp "$against" "$backup" &&
                                $TOOL "$backup" "$skel" -o "$against"

                        processed_files="$against\n$processed_files"
                fi
        done

        if [ -n "$processed_files" ] ; then
                clear
                echo "List of processed files:"
                echo "$processed_files" | sort
                echo
        fi
' {} + ; cd

# purge_empty_backups () {
#       find "$HOME" -mindepth 1 -type f -empty -name "*~" -delete
# }
