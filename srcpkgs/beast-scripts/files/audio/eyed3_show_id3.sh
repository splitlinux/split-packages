#!/bin/sh

# Supported tools are either "sent" or "less in tmux"
readonly TOOL="$1"

# When called without a parameter, the currently played track in MPD is used
readonly TRACK=${2:-"`xdg-user-dir MUSIC`/`mpc --format %file% | head -n1`"}

read_cmd='eyeD3'
# For FLAC files use metaflac (package "flac") instead
echo "${TRACK}" | grep '.flac$' && read_cmd='metaflac --list --block-type=VORBIS_COMMENT'

case "${TOOL}" in
        sent) 
                ${read_cmd} "${TRACK}" | sent - # && xdotool search --limit=1 --classname 'sent' windowactivate --sync
                ;;
        *)
                # NOTE enclosing ${TRACK} in single quotes has the command fail if the song title includes one too
                tmux new-window "${read_cmd} \"${TRACK}\" | less -r" &&
			xdotool search --limit=1 --classname "${BEAST_TERMINAL:-alacritty}" windowactivate --sync
                ;;
esac
