! Play with this setting until window contents display in comfortable sizes
! Xft.dpi=96

! scrollbar style - rxvt (default), plain (most compact), next, or xterm
URxvt.scrollstyle: plain

! Disable scrolling (tmux provides it)
URxvt*saveLines:0


! Offer multiple fonts to urxvt.
! If certain symbols are missing in one, the next is used as fallback.
! See "urxvt doesn't display correctly some special characters" at
! https://bbs.archlinux.org/viewtopic.php?pid=1349107#p1349107

URxvt.font: \
            xft:monaco:size=15, \
            xft:monaco:size=11, \
            xft:MgOpenModata:size=13, \
            xft:Symbola:size=10, \
            xft:terminus:size=13, \
            xft:monospace:size=14


URxvt.perl-ext-common:  default,matcher,clipboard
URxvt.url-launcher:     /usr/bin/xdg-open
URxvt.matcher.button:   1
URxvt.colorUL:		#4682B4

URxvt.keysym.S-Insert: perl:clipboard:paste
URxvt.clipboard.pastecmd: xsel -ob


! Colors

#define S_magenta #d33682
#define S_blue #268bd2
#define S_green #859900
#define S_base0 #839496


! To only apply colors to your terminal, for example, prefix
! the color assignment statement with its name. Example:
!
! URxvt*background: S_base03

URxvt*color4: S_blue
URxvt*color5: S_magenta


! Use the specified colour as the windows background colour [default White]; option -bg.
URxvt*background: [80]Black

! Use the specified colour as the windows foreground colour [default Black]; option -fg.
URxvt*foreground: S_base0

! Use the specified colour for the colour value n, where 0-7 corresponds to low-intensity (normal) colours and 8-15 corresponds to high-intensity
! URxvt*colorn: colour

! Use the specified colour to display bold characters when the foreground colour is the default.
! URxvt*colorBD: colour

! Use the specified colour to display italic characters when the foreground colour is the default.
! URxvt*colorIT: colour

! Use the specified colour to display underlined characters when the foreground colour is the default.
! URxvt*colorUL: colour

! If set, use the specified colour as the colour for the underline itself. If unset, use the foreground colour.
! URxvt*underlineColor: colour

! If set, use the specified colour as the background for highlighted characters. If unset, use reverse video.
! URxvt*highlightColor: colour

! If set and highlightColor is set, use the specified colour as the foreground for highlighted characters.
! URxvt*highlightTextColor: colour

! Use the specified colour for the cursor. The default is to use the foreground colour; option -cr.
! URxvt*cursorColor: colour

! Use the specified colour for the colour of the cursor text. For this to take effect, cursorColor must also be specified.
! URxvt*cursorColor2: colour

! True: simulate reverse video by foreground and background colours; option -rv. False: regular screen colours [default]; option +rv. See note in COLOURS
! URxvt*reverseVideo: boolean

! True: specify that jump scrolling should be used. When receiving lots of lines, urxvt will only scroll once a whole screen height of lines has been ..
! URxvt*jumpScroll: boolean

! True: (the default) specify that skip scrolling should be used. When receiving lots of lines, urxvt will only scroll once in a while.
! URxvt*skipScroll: boolean

! Turn on/off pseudo-transparency by using the root pixmap as background.
! URxvt*transparent: boolean
! Set to false, because transparency is taken care of by xcompmgr + transset-df
URxvt*.transparent: false

! Fade the text by the given percentage when focus is lost; option -fade.
! URxvt*fading: number

! Fade to this colour, when fading is used (see fading:). The default colour is black; option -fadecolor.
! URxvt*fadeColor: colour

! Tint the transparent background with the given colour. If the RENDER extension is not available only black, red, green, yellow, blue, magenta, cyan and ..
! URxvt*tintColor: colour

! Darken (0 .. 99) or lighten (101 .. 200) the transparent background.  A value of 100 means no shading; option -sh.
! URxvt*shading: number
URxvt*.shading: 0

! Specify background blending type; option -blt.
! URxvt*blendType: string

! Apply gaussian blur with the specified radius to the transparent background; option -blr.
! URxvt*blurRadius: number

! Set the application icon pixmap; option -icon.
! URxvt*iconFile: file

! Use the specified colour for the scrollbar [default #B2B2B2].
! URxvt*scrollColor: colour

! Use the specified colour for the scrollbars trough area [default #969696]. Only relevant for rxvt (non XTerm/NeXT) scrollbar.
! URxvt*troughColor: colour

! The colour of the border around the text area and between the scrollbar and the text.
! URxvt*borderColor: colour

! Use the specified image file for the background and also optionally specify a colon separated list of operations to modify it.
! URxvt*backgroundPixmap: file[;oplist]

! Specify the colon-delimited search path for finding background image files.
! URxvt*path: path

! Select the fonts to be used. This is a comma separated list of font names that are checked in order when trying to find glyphs for characters.
! URxvt*font: fontlist
! URxvt*boldFont: fontlist
! URxvt*italicFont: fontlist

! The font list to use for displaying bold, italic or bold italic characters, respectively.
! URxvt*boldItalicFont: fontlist

! When font styles are not enabled, or this option is enabled (True, option -is, the default), bold/blink font styles imply high intensity ..
URxvt*intensityStyles: false
