#!/bin/sh

st -f monaco:size=16 -e zsh -lc '
#colors="red\ngreen\nyellow\nblue\nmagenta\ncyan\nwhite\nblack"
colors="yellow"
cmatrix_color=`echo "${colors}" | sort --random-sort --random-source=/dev/urandom | head -n1`
cmatrix -bC ${cmatrix_color}'
