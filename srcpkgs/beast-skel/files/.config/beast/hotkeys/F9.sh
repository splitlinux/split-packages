#!/bin/sh

SCRIPT=$0 st -f monaco:size=16 -e bash -c '
	YELLOW="\033[1;33m" && echo -e $YELLOW
	
	styles=`cowsay -l | grep -v "^Cow " | sed "s#\s#\n#g"`
	style=`for f in $styles ; do echo $f ; done | sort --random-sort --random-source=/dev/urandom | tail -n1`
	cowsay -f $style I say whatever $SCRIPT tells me to.
	echo && read -sn1 key'
