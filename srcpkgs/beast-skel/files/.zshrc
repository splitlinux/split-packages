# DO NOT EDIT THIS FILE
#
# Edit ~/.zshrc.d/80-personal.sh instead or add a new file to ~/.zshrc.d/
#

if [ -d ~/.zshrc.d ] ; then
        for script in ~/.zshrc.d/?*.sh ; do
                [ -x "${script}" ] && . "${script}"

                full_path="`realpath ${script}`"
                basename="`basename ${script}`"

                # Add dynamic edit shortcuts for all scripts in zshrc.d
                alias "Vzshrc-${basename}"="${EDITOR:-nvim} `realpath ${script}` &&
			echo 'Reloading ${basename}.sh ...' && source '${full_path}'"
        done
        unset script
fi

# Add dynamic edit shortcuts for all scripts in ~/.xinitrc.d/
if [ -d ~/.xinitrc.d ] ; then
        for script in ~/.xinitrc.d/?*.sh ; do
                basename="`basename ${script}`"
                alias "Vxinitrc-${basename}"="${EDITOR:-nvim} `realpath ${script}`"
        done
        unset script
fi
