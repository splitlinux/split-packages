#!/usr/bin/env zsh

zsh_activate_prompt_theme_system () {
        autoload -Uz promptinit
        promptinit
}

zsh_activate_smartcase_completions () {
        # http://stackoverflow.com/questions/24226685/have-zsh-return-case-insensitive-auto-complete-matches-but-prefer-exact-matches
        zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
}

zsh_add_exit_status_to_prompt () {
        local separator='➤'    # '▶️' # '➤' # '🔀'
        local ok=''            # '🎶' # '🎼' # '☻'
        local x1='↖'           # '↖️' # '↗️' # '⬆️' # '💢' # '🙃' # '☹'
        local x2='↗'           # '↖️' # '↗️' # '⬆️' # '💢' # '🙃' # '☹'
        
        local zero_output="%B%F{243}${ok}%f%b"
        local nonzero_output="%B%F{1}${x1}\$?${x2}%f%b"
        local exit_status="%(?,$zero_output,$nonzero_output )"
        local hierarchy_depth="%B\${PWD//[^\/]}%b"
        PROMPT="%{$fg[$NCOLOR]%}${hierarchy_depth} %{$fg[$NCOLOR]%}%c ${exit_status}${separator} %{$reset_color%}"
        
        #local symbol="%(?,%B%F{243}${zero}%f%b,%B%F{1}${other}%f%b)"
        #RPROMPT="\$? \$symbol ${RPROMPT}"
        #RPROMPT="                                       "
}

zsh_configure_keys () {
        # Create a zkbd compatible hash;
        # To add other keys to this hash, see: man 5 terminfo
        typeset -A key

        key[Home]=${terminfo[khome]}
        key[End]=${terminfo[kend]}
        key[Insert]=${terminfo[kich1]}
        key[Delete]=${terminfo[kdch1]}
        key[Up]=${terminfo[kcuu1]}
        key[Down]=${terminfo[kcud1]}
        key[Left]=${terminfo[kcub1]}
        key[Right]=${terminfo[kcuf1]}
        key[PageUp]=${terminfo[kpp]}
        key[PageDown]=${terminfo[knp]}


        bindkey "^[[1~"         beginning-of-line
        bindkey "^[[4~"         end-of-line
        # Delete previous word with Ctrl-Backspace
        bindkey '^H'            backward-kill-word

        # Bind P and N for EMACS mode
        bindkey -M emacs '^P' history-beginning-search-backward
        bindkey -M emacs '^N' history-beginning-search-forward
        
        # For some reason data from "terminfo" is empty in st, causing "bindkey: cannot
        # bind to an empty key sequence" upon execution.
        # TODO resolve that issue
        if [ $TERM != 'st-256color' ]; then
                ## TCSH-style history search
                # bind UP and DOWN arrow keys
                #zmodload zsh/terminfo
                bindkey "${key[Up]}"     history-beginning-search-backward
                bindkey "${key[Down]}"   history-beginning-search-forward
        
                bindkey -M viins "${key[Up]}" history-beginning-search-backward
                bindkey -M viins "${key[Down]}" history-beginning-search-forward
        fi
        
        # bind k and j for VI mode
        bindkey -M vicmd 'k' history-beginning-search-backward
        bindkey -M vicmd 'j' history-beginning-search-forward
        
        # Activate VI mode
        bindkey -v
}

zsh_define_shell_history_handling () {
        # See http://zsh.sourceforge.net/Doc/Release/Options.html

        # Do not enter command lines into the history list if they are duplicates of the previous event.
        setopt HIST_IGNORE_DUPS
        # When searching for history entries in the line editor, do not display duplicates of a line previously found, even if the duplicates are not contiguous.
        setopt HIST_FIND_NO_DUPS
}

zsh_print_line_after_the_prompt () {
        {
                # TODO find out how to color the prompt itself upon exec
                preexec() {
                        BLUE='\033[0;34m'
                        local color=${BLUE} && local symbol='■' # '☐' # '⬢'
                        for ((n=0; n<COLUMNS; n++)) { printf "${color}${symbol}" ; } && printf "\n"
                        tput sgr0
                }
        }
}

zsh_print_line_before_the_prompt () {
        {
                # Inspired by https://unix.stackexchange.com/a/35485
                precmd() {
                        exit_code=$?
                
                        GRAY='\033[1;30m'
                        BLUE='\033[0;34m'
                
                        local color=${BLUE} && [[ ${exit_code} -gt 0 ]] && color=${GRAY}
                        local symbol='■'
                        #local symbol='↗️' && [[ ${exit_code} -gt 0 ]] && symbol='↙' # '↭' '↖️' '🌊'
                        #local symbol='▮' && [[ ${exit_code} -gt 0 ]] && symbol='▯'
                        #local symbol='▬' && [[ ${exit_code} -gt 0 ]] && symbol='▭'
                        #local symbol='⌧' && [[ ${exit_code} -gt 0 ]] && symbol='⮹'
                        #local symbol='■' && [[ ${exit_code} -gt 0 ]] && symbol='⌧'
                        #local symbol='■' && [[ ${exit_code} -gt 0 ]] && symbol='☐'
                        #local symbol='█'
                
                        # activate underlining
                        #tput smul
                        for ((n=0; n<COLUMNS; n++)) { printf "${color}${symbol}" ; }
                        printf "\n"
                }
                preexec() { tput sgr0 ; }
        }
}

zsh_ring_system_bell_when_long_process_finishes () {
        # Ring system bell after a process took at least 60 seconds to finish:
        autoload -Uz add-zsh-hook
        typeset -i THRESHOLD=60

        save_starttime () { starttime=$SECONDS ; }

        set_longrunning_alert () {
                if ((THRESHOLD > 0 && SECONDS - starttime >= THRESHOLD)); then
                        print '\a'
                fi
        }

        add-zsh-hook preexec save_starttime
        add-zsh-hook precmd set_longrunning_alert
}

zsh_output_text_at_start () { echo "Ahoi! Here's your shell: 🐚"; }

zsh_output_path_and_hints_upon_changing_directory () {
        function chpwd () {
		#echo "[ ${PWD} ]" | ag --color-match "1;34" "\w.*\w|$" >& 2;
		echo "[ ${PWD} ]" | GREP_COLORS='mt=33' grep --color -E " .*\w|$" >& 2;

		local hints_file=.hints
		[ -f $hints_file ] && file --dereference --brief --mime-type $hints_file | \
			grep -w 'text/plain' >/dev/null && cat $hints_file
	}
}


activate_chruby_version_manager () {
	local chruby_script=/usr/share/chruby/chruby.sh

	if [ -f "$chruby_script" ] ; then
		# NOTE After installing new Rubies, you must restart the shell in order to have chruby recognize them.
		source /usr/share/chruby/chruby.sh

		# Auto-switch current Ruby version upon encountering .ruby-version file.
		source /usr/share/chruby/auto.sh
	
		# Use oldest available ruby version by default
		chruby `chruby | head -n1 | sed 's#*##g'`
	fi
}

activate_node_version_manager () {
        [ -z "$NVM_DIR" ] && export NVM_DIR="$HOME/.nvm"

        # nvm.sh will load the default nodejs version as set by 'nvm alias default v6.10'
        source /usr/share/nvm/nvm.sh
}

set_environment_variables () {
        export HISTSIZE=10000000
        export SAVEHIST=$HISTSIZE

        export EDITOR=nvim
        export BROWSER=tor-browser

        # use name of current directory as console session name
        #export PS1=$PS1"\[\e]30;\H:\w\a\]"

        # colorize `ls` output
        export LS_OPTIONS='--color=auto' && eval `dircolors`

        #GOPATH=":$HOME/.go"
        # The default binary path for applications installed via pipsi/pip/pip2 install --user
        PYTHONPATH=":$HOME/.local/bin"
        PATH="/usr/bin/beast:${PATH}${PYTHONPATH}${GOPATH}"

        [[ -z ${XDG_CONFIG_HOME} ]] && export XDG_CONFIG_HOME="${HOME}/.config"
        WLAN_DEV=`iw dev 2> /dev/null | grep Interface | awk '{ print $NF }'`
}


if [ "`ps -p $$ -ocomm=`" = "zsh" ] ; then
        activate_chruby_version_manager
        #activate_node_version_manager
        set_environment_variables

        zsh_add_exit_status_to_prompt
        #zsh_activate_prompt_theme_system
        zsh_configure_keys
        zsh_define_shell_history_handling
        zsh_output_path_and_hints_upon_changing_directory
        #zsh_output_text_at_start
        #zsh_print_line_before_the_prompt
        zsh_print_line_after_the_prompt
        zsh_ring_system_bell_when_long_process_finishes
        zsh_activate_smartcase_completions
fi
