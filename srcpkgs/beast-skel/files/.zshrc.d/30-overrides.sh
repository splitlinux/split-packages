#!/usr/bin/env zsh

# This file is exclusively intended for substituting known commands with others
# or a variant of the original command with certain options activated.

# Prompt before overwrite
alias cp='cp --interactive'

# Make your gist private
alias gist='gist --private'

# Wait for all killed processes to die
alias killall='killall --wait'

# Colorize man
man () {
        black=0 ; red=1; green=2; yellow=3; blue=4
        magenta=5; cyan=6; white=7; grey=8; light_blue=33

        blink     () { tput blink; }
        bold      () { tput bold; tput setaf $yellow; }
        standout  () { tput setaf $black; tput setab $yellow; }
        underline () { tput smul; tput setaf $light_blue; }
        reset     () { tput sgr0; }

        LESS_TERMCAP_mb=`blink` \
        LESS_TERMCAP_md=`bold` \
        LESS_TERMCAP_me=`reset` \
        LESS_TERMCAP_so=`standout` \
        LESS_TERMCAP_se=`reset` \
        LESS_TERMCAP_us=`underline` \
        LESS_TERMCAP_ue=`reset` \
        command man "$@"
}

# Play audio in console only
alias mpv='mpv --audio-display=no'

# --interactive Prompt before overwrite
alias mv='mv --interactive'

# Generate stronger passwords
alias pwgen='pwgen --numerals --capitalize --symbols 20'

# Prompt before every removal
alias rm='rm -i'

# Show numerical addresses instead of trying to determine symbolic host names
alias route='route -n'

# Allow opening files with SPACE key
alias rover='ROVER_PAGER=xdg-open rover'

# Search the given directories recursively 
# Write list of all marked files to standard output when quitting.
# Set scale mode according to [f]it
alias sxiv='sxiv -rosf'

# Name of the field on which tasks will be sorted
alias top='top -o "%CPU"'

# Use nvim in place of vi
alias vi='nvim'

# Do not display the legal disclaimers some registries like to show you
alias whois='whois -H'
