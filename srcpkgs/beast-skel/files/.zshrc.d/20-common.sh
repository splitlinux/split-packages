#!/usr/bin/env zsh


# BD..
alias trash='BDtrash_resource'


# 7z
unpack_archive () { name="$*" && 7z x -o"${name%%.*}" "$name" ; }
alias unp=unpack_archive


# alias / declare
find_alias ()    { alias      | grep -i "$*" }
find_function () { declare -f | grep '() {' | grep -i "$*" }
find_function_or_alias () { find_function "$*" ; find_alias "$*" ; }
show_function () {
        matching_functions=`find_function "$*" | awk '{ print $1 }'`

        echo $matching_functions | while read f ; do
                declare -f "$f"
        done
}
unalias a 2> /dev/null ; alias a=find_function_or_alias
alias af=show_function


# abcde
alias rip_audio_cd='abcde'


# ag
alias ag_search_textile='ag --file-search-regex ".textile$"'


# beep
alias bp='beep_after'
alias beep_every_10s='echo "Ensure that pcspkr is loaded (sudo modprobe pcspkr)" && beep -f 800 -l 500 -r 9999 -d 10000'
alias beep_every_15s='echo "Ensure that pcspkr is loaded (sudo modprobe pcspkr)" && beep -f 800 -l 500 -r 9999 -d 15000'


# bundle
alias Bi='bundle install'
alias Bl='bundle list'
alias Bs='bundle show'
alias Bu='bundle update --no-color | grep --color -E "^Installing .*$|$"'


# cargo / rustc
alias Rb='cargo build'
alias Rdoc='cargo doc --open'
alias Rexplain='rustc --explain'
alias Rf='cargo search'
alias Rr='cargo run'
alias Rt='cargo test'


# cat
alias list_block_devices='ls /sys/block/sd*/device/model && cat /sys/block/sd*/device/model'


# cd
alias CD="cd `xdg-user-dir DOWNLOAD`"
alias Cd="cd `xdg-user-dir DOCUMENTS`"
alias Cm="cd `xdg-user-dir MUSIC`"
alias Cp="cd `xdg-user-dir PICTURES`"
alias Cv="cd `xdg-user-dir VIDEOS`"


# clear
alias clr='clear && printf "\e[3J"'
alias clrt='clear && tmux clearhist'


# crunch
alias anagram_permutations='crunch 0 0 -p'


# curl
alias myip="echo -n 'HTTPS: ' && curl -s https://check.torproject.org/api/ip | jq"
alias IKNOWWHATIAMDOING_pipe_to_ixio_PUBLIC_pastebin="curl -F 'f:1=<-' ix.io"


# date
alias date_utc='TZ=UTC date'
alias date_time='date +%Y%m%d_%H%M%S'
alias timestamp_from_date='date +%s --date'


# df
alias dfh='df -h'


# diff
alias diffy='diff -y'


# dmesg
alias get_info_about_kernel_panic='echo "The following command will follow the dmesg log and print it to a file.\nLet it run until the kernel panics and check the log after booting:\nnohup dmesg -w > ~/dmesg.log &"'


# docker
alias D='docker'
alias Dbuild='docker build -t'
alias Dc='docker container'
alias Dcattach='docker container attach'
alias Dcls='docker container ls'
alias Dcprune='docker container prune'
alias Dcrestart='docker container restart'
alias Dcrm='docker container rm'
alias Dcstart='docker container start'
alias Dcstop='docker container stop'
alias De='docker exec'
alias Di='docker images'
alias Dinspect='docker inspect'
alias Dlfollow='docker logs --tail 50 --follow --timestamps'
alias Dn='docker network'
alias Dnconnect='docker network connect'
alias Dninspect='docker network inspect'
alias Dr='docker run'


# du
alias duh='du -chsx * | sort -h'
alias duha='du -chsx --apparent-size *'
alias dus='du -csx * | sort -n'


# echo
alias ring_system_bell="echo -ne '\007' || print '\a'"


# espeak
alias say='espeak -vmb-en1'


# exiftool
alias remove_exif_metadata_from='exiftool -all='


# exit
alias x='exit'


# export
alias english_please='export LANG=en_US.UTF-8'


# fc-list
alias list_available_fonts='fc-list'


# fdupes
alias fdupes_recursive="fdupes --summarize --recurse ./ && echo Run \'fl\' to get the file names or \'fld\' to be prompted for their deletion."
alias fl="fdupes -r ./ && echo Run \'fld\' to be prompted for file deletion."
alias fld='fdupes -rd ./'


# file
alias file_mimetype='file --dereference --brief --mime-type'


# find
find_file_or_directory () { find ./ -iname "*$**"; }
alias f=find_file_or_directory
alias find_accessed_between_5_and_3_years_ago="find ./ -atime -$((365*5)) -atime +$((365*3))"
alias find_all_files_hardlinked_to_the_file='find -mount -samefile'
alias find_delete_empty_directories='find . -mindepth 1 -type d -empty -print -delete'
alias find_delete_empty_nomedia_files='find . -mindepth 1 -type f -empty -name ".nomedia" -print -delete'
alias find_employed_firmware_blobs='find /lib/firmware -atime -1'
alias find_hardlinks="find ./ -type f -links +1 -exec ls -ahl {} \;"
alias find_larger_than_300M="find ./ -type f -size +300M -exec ls -ahl {} \; | grep -v '/home/js/'"
alias find_last_accessed_over_5_years_ago="find ./ -atime +$((365*5))"
alias find_where_data_changed_within_last_3_days="find ./ -type f -mtime -3"
alias find_where_data_changed_within_last_10_days="find ./ -type f -mtime -10"
alias find_where_data_changed_within_last_30_days="find ./ -type f -mtime -30"
alias find_where_data_changed_more_than_5_years_ago="find ./ -type f -mtime +$((365*5))"


# findimagedupes
alias find_duplicate_images='findimagedupes'


# fswebcam
alias fswebcam_take_snapshot='fswebcam --font "Monofonto:30" --title "`xdotool getwindowfocus getwindowname`" --subtitle "`whoami`@`hostname`" --info "`uptime --pretty`" --resolution 1920x1080 --save $(mktemp --tmpdir "`date +%Y%m%d_%H%M%S_fswebcam_1920x1080_XXX.jpg`")'


# vetoz - visit examine textile open zoom
alias v='chdir_via_finddb_files'
#alias C='clip_dir_from_finddb_files'
alias z='chdir_via_finddb_directories'
alias Z='clip_dir_from_finddb_directories'
alias e='open_via_finddb'
alias E='clip_from_finddb'
alias o='open'
alias s='open_textile_in_sent_via_finddb'
alias t='open_textile_via_finddb'
alias T='clip_textile_from_finddb'
show_realpath_of () { [ -e "$@" ] && echo -n "'`realpath $@`'" | xsel -ib && echo "'`realpath $@`'" ; }
alias O='show_realpath_of'


# tee
alias rotate_consoles_level='echo 0 | sudo tee /sys/class/graphics/fbcon/rotate_all'
alias rotate_consoles_right='echo 1 | sudo tee /sys/class/graphics/fbcon/rotate_all'
alias rotate_consoles_down='echo 2 | sudo tee /sys/class/graphics/fbcon/rotate_all'
alias rotate_consoles_left='echo 3 | sudo tee /sys/class/graphics/fbcon/rotate_all'
alias set_IFS_input_field_separator_to_newline="echo 'Need that for filling an array in zsh? You should use the f expansion flag instead: array_of_lines=(\${(@f)\`some_cmd\`})\nSee http://zsh.sourceforge.net/Doc/Release/Expansion.html for details.'"


# todoman
alias Tcancel='todo cancel'
alias Td='todo delete'
alias Tdone='todo done'
alias Te='todo edit'
alias Tl='todo list'
alias Tm='todo move'
alias Tn='todo new'
alias Ts='todo show'


# torsocks
alias TOR='torsocks -a 172.18.0.2 -P 9050 --isolate --quiet'


# tput
alias fix_terminal='echo -e "\033c" ; stty sane; setterm -reset; reset; tput reset; clear'
alias show_colordepth='echo "`tput colors` - Current \$TERM is ${TERM}"'


# gem
alias GEMbuild='gem build *.gemspec'
alias GEMinstall='gem install *.gem'
alias GEMuninstall='gem uninstall'
alias GEMpush='gem push *.gem'
alias GEMyank='gem yank'


# git
alias G_add_and_commit_porcelain='git add . && git status --porcelain --untracked-files=no | git commit -F -'
alias G_delete_current_branch='branch=`git rev-parse --abbrev-ref HEAD` && Go master && Gbd "${branch}"'
alias G_open_all_files_from_history_that_contain='git grep -O'
alias G_merge_current_branch_into_current_master_and_push='branch=`git rev-parse --abbrev-ref HEAD` && Go master && Gpull origin master && Go "${branch}" && Grebase master && Go master && Gm "${branch}" && GPUSH origin master && Go "${branch}"'
alias G_rebase_current_branch_on_current_master='branch=`git rev-parse --abbrev-ref HEAD` && Go master && Gpull origin master && Go "${branch}" && Grebase master'
alias G_remove_untracked_files_and_directories='git clean -d'
alias G_revert_master_to_upstream='echo "This is dangerous. Do it yourself:\ngit remote update && git reset --hard upstream/master --"'
alias Ga='git add'
alias Gb='git branch'
alias Gbd='git branch -d'
alias Gbe='git branch --edit-description'
alias Gbm='git branch --move'
alias Gbv='git branch -v'
alias Gc='git commit'
alias Gca='git commit --amend'
alias Go='git checkout'
alias Gob='git checkout -b'
alias Gcp='git cherry-pick'
alias Gcommitporcelain='git --no-optional-locks status --porcelain --untracked-files=no | git commit -F -'
alias Gconfig='git config'
alias Gclone='git clone'
alias Gd='git diff'
alias Gdlast='git diff HEAD@{1} HEAD@{0}'
alias Gdlast_nameonly='git diff --name-status HEAD@{1} HEAD@{0}'
alias Gi='git init'
alias Glist_contributors_by_commit_count='git shortlog --summary --numbered --email'
alias Gl='git log'
alias Glp='git log --minimal --patch-with-raw'
alias Glr='git log --raw'
alias Gm='git merge'
alias Gmv='git mv'
# Git pull for fast-forwards only:
# http://stackoverflow.com/questions/15316601/in-what-cases-could-git-pull-be-harmful/15316602#15316602
alias Gpull='git pull --ff-only'
alias Gpullupstream='git pull --ff-only upstream'
alias Gpullupstreammaster='git --ff-only pull upstream master'
# Written in capitals in order to avoid confusion with Gpull
alias GPUSH='git push'
alias Gr='git remote'
alias Grebase='git rebase'
alias Grrename='git remote rename'
alias Grv='git remote -v'
alias GresetHEAD='git reset HEAD'
alias Grevert='git revert'
alias Grevert_undo_specific_commit='Grevert'
alias Grm='git rm'
alias Gs='git status'
alias Gstash='git stash'
alias Gsubmodule='git submodule'
alias Gsubmodule_remove='echo -e "Please do this manually like so:\n\n1. Delete the relevant section from .gitmodules\n2. Delete the relevant section from .git/config\n3. Run git rm --cached path_to_submodule (without trailing slash)\n4. Commit and delete the now untracked submodule files\n\nSource: http://stackoverflow.com/questions/1260748/how-do-i-remove-a-git-submodule"'
alias Gunstash='git stash apply'
alias Gt='git tag -n1'
alias Gtd='git tag -d'
alias Grevert_a_single_file_to_HEAD='git checkout -- '
alias Grestore_staged='git restore --staged'
alias Gshow_config='echo "GLOBAL:" && git config --global -l ; echo -e "\nLOCAL:" && git config --local -l'
alias Gundo_last_commit='git reset --soft "HEAD^"'


# gngeo
# NeoGeo emulator with SDL hardware surface (-H), scaled, with sound and in
# fullscreen.
# Order for --p1key definitions is A, B, C, D, START, COIN, X AXIS, Y AXIS
# Key codes can be found in gngeo by pressing F4 followed by a key.
#alias gngeo='gngeo -H --scale=2 --sound -f --p1key=102,100,115,97,103,69'
alias gngeo='gngeo -H --scale=3'


# gpg
gpg_set_expiration_for_key () { gpg --edit-key "$1" expire ; }
gpg_add_subkey () { gpg --edit-key "$1" addkey ; }
alias gpg_create_key='gpg --full-gen-key'
alias gpg_decrypt='gpg --decrypt-files'
alias gpg_encrypt_with_passphrase_only='gpg --symmetric --cipher-algo AES256'
alias gpg_encrypt_for_recipient='gpg --encrypt --recipient'
# The following is dangerous since GPG simply uses the first key in the list.
# Explicitly specify a recipient instead.
#alias gpg_encrypt_for_self='gpg --encrypt --default-recipient-self'
alias gpg_get_key_from_keyserver='gpg --recv-keys --keyserver pgp.mit.edu'
alias gpg_import_pubkey='gpg --import'
alias gpg_list_public_keys='gpg --list-public-keys'
alias gpg_list_secret_keys='gpg --list-secret-keys'
alias gpg_list_signatures_of='gpg --list-sigs'
# When signing, the --local-user argument may be useful. It tells the program AS
# WHO to sign the key as.
alias gpg_mark_key_as_authentic='gpg --lsign-key'
alias gpg_export_public_keys_ascii_named='gpg --export-options export-minimal --export --armor'
alias gpg_export_public_keys_binary_named='gpg --export-options export-minimal --export'
alias gpg_export_secret_keys_ascii_named='gpg --export-options export-minimal --export-secret-keys --armor'
alias gpg_export_secret_keys_binary_named='gpg --export-options export-minimal --export-secret-keys'
alias gpg_show_contents_of_key_file='gpg --list-packets'
alias gpg_show_contents_of_key_file_short='gpg'


# grep
alias grep_text_files_recursively='grep --binary-files=without-match --dereference-recursive --ignore-case'
alias g=grep_text_files_recursively
alias grep_blue="GREP_COLOR='1;34' grep --color=always"
alias grep_cyan="GREP_COLOR='1;36' grep --color=always"
alias grep_green="GREP_COLOR='1;32' grep --color=always"
alias grep_grey="GREP_COLOR='1;30' grep --color=always"
alias grep_magenta="GREP_COLOR='1;35' grep --color=always"
alias grep_red="GREP_COLOR='1;31' grep --color=always"
alias grep_white="GREP_COLOR='1;37' grep --color=always"
alias grep_yellow="GREP_COLOR='1;33' grep --color=always"


# history
#find_in_history () { history | grep "$*"; }
find_in_history () {
        TMP=`mktemp`
        sed 's#^: ..........:.;##g' ~/.zsh_history | awk '!x[$0]++' | tac > $TMP
        BDfuzzy_find $TMP $@
        rm $TMP
}
alias h=find_in_history


# hledger
readonly HLEDGER_COMMON_FLAGS='--file ./hledger.journal' 2> /dev/null
alias H="hledger ${HLEDGER_COMMON_FLAGS}"
alias Ha="hledger accounts ${HLEDGER_COMMON_FLAGS}"
alias Hy="hledger activity ${HLEDGER_COMMON_FLAGS}"
alias Hb="hledger balance ${HLEDGER_COMMON_FLAGS}"
alias Hbv="hledger balance ${HLEDGER_COMMON_FLAGS} --value=now --file market-prices/in-usd.journal"
alias Hbs="hledger balancesheet ${HLEDGER_COMMON_FLAGS}"
alias Hc="hledger cashflow ${HLEDGER_COMMON_FLAGS}"
alias Hi="hledger incomestatement ${HLEDGER_COMMON_FLAGS}"
alias Hn="hledger add ${HLEDGER_COMMON_FLAGS}"
alias Hp="hledger print ${HLEDGER_COMMON_FLAGS}"
alias Hr="hledger register --width $COLUMNS,`expr $COLUMNS - 80`  ${HLEDGER_COMMON_FLAGS}"
alias Hu='hledger_update_journal_from_csv'
alias HUI="hledger-ui --theme=greenterm ${HLEDGER_COMMON_FLAGS}"
alias HUIV="hledger-ui --theme=greenterm ${HLEDGER_COMMON_FLAGS} --value=now,USD --file market-prices/all.journal"


# iconv
alias iconv_iso88591_to_utf8='iconv -f ISO-8859-1 -t UTF-8'


# ifuse
alias mount_iphone_or_ipad='mkdir ~/mnt && ifuse ~/mnt'


# imagemagick
alias convert_with_high_quality='convert -density 300 -quality 100'
alias mogrify_to_800x600='mogrify -geometry 800x600 -quality 80'
alias mogrify_to_1200x1000='mogrify -geometry 1200x1000 -quality 80'
alias mogrify_to_1600x1200='mogrify -geometry 1600x1200 -quality 80'
alias preview_font_from_file='display'


# inkview
alias view_svg_file='inkview'


# iotop
alias monitor_disk_io='sudo iotop -oPa'


# ip / iw
alias wireless_connect_unencrypted="sudo iw dev ${WLAN_DEV} connect"
#alias wireless_connect_wep="iw dev ${WLAN_DEV} connect ${1} key 0:${2}"
#alias wireless_connect_wpa="essid=`iw dev ${WLAN_DEV} link | grep SSID | awk '{ print $NF }'` && wpa_supplicant -i ${WLAN_DEV} -c /etc/wpa_supplicant/${essid}.conf"
alias wireless_down="sudo ip link set ${WLAN_DEV} down && ip link show ${WLAN_DEV}"
alias wireless_get_ip_dhcpcd="dhcpcd ${WLAN_DEV}"
alias wireless_get_ip_dhclient="dhclient ${WLAN_DEV}"
alias wireless_link_status="ip link show ${WLAN_DEV} && iw dev ${WLAN_DEV} link"
alias wireless_scan_for_access_points="sudo iw dev ${WLAN_DEV} scan | grep --color -E 'signal:|SSID:|$'"
alias wireless_station_dump="iw dev ${WLAN_DEV} station dump"
alias wireless_up="sudo ip link set ${WLAN_DEV} up && ip link show ${WLAN_DEV}"
#cd /etc/wpa_supplicant && for c in *.conf; do
#        conf="`echo $c|sed 's/.conf$//g'`" && alias "WPA${conf}"="wireless_connect_wpa ${conf}"
#done && cd '${OLDPWD}'
alias list_network_devices='ip a'


# iwconfig
alias wireless_connect_unencrypted_iwconfig="iwconfig ${WLAN_DEV} essid"


# iwlist
alias wireless_scan_for_access_points_iwlist="sudo iwlist ${WLAN_DEV} scan"


# jekyll
alias Jb='bundle exec jekyll build'
alias Js='bundle exec jekyll serve --watch --trace'


# jmtpfs
#alias mtp_list_devices='simple-mtpfs --list-devices'
alias mtp_mount_device='mkdir -p ~/mtp && jmtpfs ~/mtp'
alias mtp_umount_device='fusermount -u ~/mtp && rmdir ~/mtp'


# jpegtran
alias jpegtran_for_web='jpegtran -copy comments -optimize'


# khard
alias K='khard'
alias Kb='khard birthdays'
alias Kd='khard delete'
alias Ke='khard edit'
alias Kl='khard list'
alias Km='khard move'
alias Kn='khard new'
alias Ks='khard show'


# kill
alias dd_report_progress='kill -USR1 `pidof dd`'


# killall / pkill
#alias X='killall'
X () { pkill --uid "${USER}" --full --exact "$@ .*" ; }
alias X9='killall -9'
alias Xu='sudo pkill -u'


# lpadmin // lpinfo // lpoptions
alias lp_list_available_printer_drivers='lpinfo -m'
alias lp_list_available_printers='sudo lpinfo -v'
alias lp_list_printer_stati='lpstat -s && echo '' && lpstat -l'
alias lp_list_print_queue='lpq'
alias lp_delete_print_job='lprm'
alias lp_set_default_printer='lpoptions -d'
alias lp_list_options_for_printer='lpoptions -l -p'


# ls
list_contents_by_last_change () {
        tree -icCDLN 1 $@ | grep -v '^[0-9]\+ director.*, [0-9]\+ file'
}
list_directories () {
        git rev-parse --is-inside-work-tree &>/dev/null &&
        k --directory $@ || k --directory --no-vcs $@ ;
}
ls_via_k () {
        git rev-parse --is-inside-work-tree &>/dev/null &&
                ( k --almost-all --directory $@ && k --almost-all --no-directory $@ ) ||
                        (
                                k --almost-all --directory --no-vcs $@ &&
                                        k --almost-all --no-directory --no-vcs $@
                        ) ;
}
type k &>/dev/null && alias l='ls_via_k' || alias l='ls'
#alias l='ls $LS_OPTIONS -lA'
alias lh='ls -lh'
alias lhidden='ls -d .*'
alias ls="ls $LS_OPTIONS"
alias lsd=list_directories
#alias lsd='ls -l | grep "^d"'
alias lt=list_contents_by_last_change
#alias lt='ls -l --sort=time --reverse'


# lshw
alias list_disks='lshw -class disk || list_block_devices'


# lxc
alias Lattach='sudo lxc-attach'
alias Lfreeze='sudo lxc-freeze'
alias Lstart='sudo lxc-start'
alias Lstop='sudo lxc-stop'
alias Lunfreeze='sudo lxc-unfreeze'
alias lxc_create_ubuntu_container_named='sudo lxc-create -t ubuntu -n'
alias lxc_start_container='sudo lxc-start -d -n'
alias lxc_enter_container='sudo lxc-console -n'
alias lxc_stop_container='sudo lxc-stop -n'
alias lxc_destroy_container='sudo lxc-destroy -n'


# mencoder
alias mencoder_ogv_to_avi='mencoder -o /tmp/mencoder.avi -oac none -ovc lavc -idx'
alias mencoder_record_divx='mencoder tv:// -tv driver=v4l2:width=640:height=480:outfmt=uyvy:fps=25 -nosound -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:turbo:vbitrate=1200:keyint=15 -o'


# mimeopen
alias oo='mimeopen'


# mkdir
alias mkd='mkdir'


# mount
find_in_mount () { mount | grep "$*"; }
alias m=find_in_mount
alias mount_nullfs='sudo mount --bind'


# mpv
alias mpv_fast_forward_loud='~/skripte/multimedia/mpv_fast_forward_loud.sh'
alias mpv_scaletempo='mpv -af=scaletempo'
alias mpv_louder='mpv --volume-max=400 --volume=200'
alias mpv_play_audio_cd='mpv cdda://'
alias mpv_mirror='mpv tv:// --tv-width=1920 --tv-height=1080 --vf mirror'
alias mpv_record_jpeg_photosession='mpv tv:// --tv-width=1920 --tv-height=1080 --vf mirror --ao=null --fps 0.5 --vo=image --vo-image-format=jpg'
alias M1610='mpv --monitoraspect=16:10'
alias M168='mpv --monitoraspect=16:8'
alias M169='mpv --monitoraspect=16:9'


# mutt
alias muttr='mutt -R'


# ncal
THIS_YEAR=`date +%Y`
NEXT_YEAR=`date --date next-year +%Y`
for m in jan feb mar apr may jun jul aug sep oct nov dec; do
        alias $m="ncal $m ${THIS_YEAR}"
        alias "$m${NEXT_YEAR}"="ncal $m ${NEXT_YEAR}"
done
alias date_of_easter='ncal -e'


# nethogs
alias monitor_network="sudo nethogs ${WLAN_DEV}"


# netstat
alias open_ports='sudo netstat -tulpen'
alias open_ports_excluding_127_0_0_1='sudo netstat -tulpen | grep -v "127.0.0.1"'
alias listen_ports='sudo netstat -lnp'


# nmap
alias nmap_detect_os_verbosity='sudo nmap -O -v'
alias nmap_detect_os_via_version_scan='sudo nmap -sV -O -v'


# open
alias o='open'


# openssl
alias openssl_show_contents_of_certificate='openssl x509 -text -noout -in '
alias openssl_show_contents_of_csr='openssl req -noout -text -in '
alias openssl_test_starttls_on_imap='openssl s_client -starttls imap -connect' # mail.example.org:143
alias openssl_test_starttls_on_smtp='openssl s_client -starttls smtp -crlf -connect' # mail.example.org:25


# pacmd
if type pacmd &>/dev/null; then
        alias pulseaudio_list_devices='pacmd list-sinks ; echo "\n" ; pactl list short sinks'
        alias pulseaudio_mixer='pavucontrol'
        alias pulseaudio_set_default_device='pacmd set-default-sink'
        alias sink0='pacmd set-default-sink 0'
        alias sink1='pacmd set-default-sink 1'
        alias sink2='pacmd set-default-sink 2'
fi


# pandoc
alias pandoc_html_textile='pandoc -f html -t textile'
alias pandoc_markdown_textile='pandoc -f markdown -t textile'
alias pandoc_textile_html='pandoc -f textile -t html5 --email-obfuscation=none'
alias pandoc_textile_html_standalone='pandoc -f textile -t html5 --standalone --email-obfuscation=javascript'
alias pandoc_textile_html_standalone_selfcontained='pandoc -f textile -t html5 --standalone --self-contained --email-obfuscation=javascript'
alias pandoc_textile_markdown='pandoc -f textile -t markdown'
alias pandoc_textile_pdf='pandoc_textile_to_pdf'


# pass
alias Pc='BDmanage_passwords clip'
alias Pn='BDmanage_passwords "add -m"'
alias Pd='BDmanage_passwords rm'
alias Pe='BDmanage_passwords edit'
alias Pf='BDmanage_passwords find'
alias Pg='pass generate --no-symbols'
alias Pgrep='pass grep'
alias Pm='pass mv'
alias Pp='BDmanage_passwords clippass'
alias Ps='BDmanage_passwords show'
alias Pv='BDmanage_passwords visit'


# pdf*
alias rotate_pdf_right='pdf270'


# perl-rename
alias rn='perl-rename'


# ping
alias P='echo "TESTING CONNECTIVITY (ping)" && sudo ping -c1 9.9.9.9 | grep --color -E ".*icmp_seq.*|$" && echo "\n\nTESTING NAME RESOLUTION (dig/drill)" && { dig +short github.com || drill github.com } | grep --color -E ".*\..*\..*\..*|$" ; echo'


# printf
alias hex2dec='printf "%d\n"'
# Prepend your value with 0x
alias dec2hex='printf "%x\n"'


# proc
alias list_soundcards='cat /proc/asound/cards'


# pry
alias pryr='pry -r ./config/environment'


# ps
find_in_ps () { ps -eo %cpu,%mem,pid,user,args --sort %cpu | grep "$*"; }
ps_how_long_has_process_been_running () { ps -o etime= -p "$1" ; }
ps_when_was_process_started () { ps -o lstart= -p "$1" ; }
alias p=find_in_ps
alias ps_print_process_tree='ps -ejH'


# psql
alias psql_shell_for_database='sudo -u postgres psql'
alias psql_list_databases='psql -l'
alias psql_activate_hstore_for_future_databases='sudo -u postgres psql template1 -c "CREATE EXTENSION IF NOT EXISTS hstore"'


# pxl
alias show_image_in_terminal_pxl='pxl'


# q


# redshift
alias toggle_redshift='pkill -USR1 redshift'
alias reset_redshift='redshift -m vidmode -x'


# rmdir
alias rmd='rmdir'
alias remove_empty_directories='find ./ -type d -exec rmdir {} +;'


# rsync
alias rsync_fast='rsync --rsh="ssh -c blowfish"'
alias rsync_for_vfat_and_ntfs='rsync -rtv --modify-window=1'


# sc-im
alias sc='sc-im'
alias sc-im_open_csv_semicolon='sc-im --txtdelim=";"'


# scanimage
alias sane_scanimage_list_devices='scanimage --list-devices'
alias sane_scanimage_show_available_options='scanimage --all-options'


# screen
alias screen_list='screen -list'
alias screen_resume='screen -r'
alias screen_create_shared='screen -S shared'
alias screen_attach_to_attached='screen -x'
alias screen_serial_9600='screen /dev/ttyS0 9600'
alias screen_serial_19200='screen /dev/ttyS0 19200'
alias screen_serial_38400='screen /dev/ttyS0 38400'
alias screen_serial_115200='screen /dev/ttyS0 115200'


# scrot
alias scrot_focussed_after_2_seconds="cd `xdg-user-dir DOWNLOAD` && sleep 2 && scrot -u && cd '${OLDPWD}'"


# sdcv
alias def='/usr/bin/sdcv'


# sort
alias rand='sort --random-sort --random-source=/dev/urandom'


# split
alias fill_current_dir_with_zeros='split -b 1000m < /dev/zero ; sync && rm -i x*'


# ss
alias whats_listening_on_ip='ss -a src'


# ssh
alias ssh_with_password='ssh -o PreferredAuthentications="password"'
alias ssh_without_hostkey_checking="ssh -o 'UserKnownHostsFile /dev/null' -o 'StrictHostKeyChecking no'"


# ssh-keygen
alias show_ssh_public_key_fingerprint_md5='ssh-keygen -E md5 -lf ~/.ssh/id_rsa.pub'


# su
alias su_with_x='sux'
alias su_execute_as_www='su -m www -c'


# sudo
alias sudo_with_x="xhost local:root && sudo DISPLAY=${DISPLAY}"


# tac
alias reverse_sort_file='tac'


# tail
alias Tcups='tail -fn100 /var/log/cups/*_log'
alias Tmail='tail -fn50 /var/log/mail.log'


# tar
alias tar_create_bzip2='tar cjf'
alias tar_extract='tar xvf'
alias tar_list_contents='tar tvf'


# tmux
alias tmux_autorename='tmux set-window-option automatic-rename on'
alias tmux_list_layouts='tmux list-windows'
alias tmux_toggle_statusbar='tmux set status'


# u


# vi / $EDITOR
alias Vasoundrc="$EDITOR ~/.asoundrc"
alias Vconky="$EDITOR ~/.conkyrc"
alias Vcrypttab="sudo $EDITOR /etc/crypttab"
alias Vdovecot="sudo $EDITOR /etc/dovecot/dovecot.conf"
alias Vfstab="sudo $EDITOR /etc/fstab"
alias Vfeh_keys="$EDITOR ${XDG_CONFIG_HOME:-~/.config}/feh/keys"
alias VGemfile="$EDITOR Gemfile"
alias Vgroup="sudo $EDITOR /etc/group"
alias Vgrub_custom="sudo $EDITOR /etc/grub.d/40_custom"
alias Vhints="$EDITOR ./.hints && cd ./"
alias Vhistory="[ -e ~/.zsh_history ] && $EDITOR ~/.zsh_history"
alias Vhosts="sudo $EDITOR /etc/hosts"
alias Vkeynav="$EDITOR ~/.keynavrc"
alias Vkhal="$EDITOR ${XDG_CONFIG_HOME:-~/.config}/khal/config"
alias Vkhard="$EDITOR ${XDG_CONFIG_HOME:-~/.config}/khard/khard.conf"
alias Vlinopen="$EDITOR ~/.linopenrc"
alias Vmpd="sudo $EDITOR /etc/mpd.conf"
alias Vmpv="$EDITOR ${XDG_CONFIG_HOME:-~/.config}/mpv/mpv.conf"
alias Vmpvinput="$EDITOR ${XDG_CONFIG_HOME:-~/.config}/mpv/input.conf"
alias Vmuttrc="$EDITOR ~/.mutt/muttrc"
alias Vpasswd="sudo $EDITOR /etc/passwd"
alias Vsamba="sudo $EDITOR /etc/samba/smb.conf"
alias Vsnippets="$EDITOR ${XDG_CONFIG_HOME:-~/.config}/beast/snippets"
alias Vssmtp="sudo $EDITOR /etc/ssmtp/ssmtp.conf"
alias Vsxiv_keyhandler="$EDITOR ${XDG_CONFIG_HOME:-~/.config}/sxiv/exec/key-handler"
alias Vtodo="$EDITOR '+/ii)\.' ~/todo.textile"
alias Vtodoman="$EDITOR ${XDG_CONFIG_HOME:-~/.config}/todoman/todoman.conf"
alias Vuserdirs="$EDITOR ${XDG_CONFIG_HOME:-~/.config}/user-dirs.dirs"
alias Vurxvt="$EDITOR ~/.Xresources && xrdb ~/.Xresources && xrdb -query -all"
alias Vvdirsyncer="$EDITOR ${XDG_CONFIG_HOME:-~/.config}/vdirsyncer/config"
alias Vviminit="$EDITOR + ${XDG_CONFIG_HOME:-~/.config}/nvim/init.vim"
alias Vvimpcrc="$EDITOR + ${XDG_CONFIG_HOME:-~/.config}/vimpc/vimpcrc"
#alias Vvimrc="$EDITOR + ${XDG_CONFIG_HOME:-~/.config}/.vimrc"
alias Vvimrc_mail="$EDITOR + ~/.vim/after/ftplugin/mail.vim"
alias Vxinitrc="$EDITOR ~/.xinitrc '+set syn=sh'"


# vim
alias vim_renamer="$EDITOR +Renamer"
alias rename_files_in_current_directory=vim_renamer
alias vim_bundle_install="$EDITOR +PluginInstall +qall"
alias vundle_install='vim_bundle_install'


# virsh
alias virsh_list_qemu_system_domains='virsh --connect qemu:///system list --all'


# wget
alias mirror_website='wget --mirror --convert-links --adjust-extension --wait=1 --recursive --level=0 --execute robots=off'


# wodim
alias burn_iso="mount | grep '/dev/cdrom' || wodim -v -sao dev=/dev/cdrom"


# xbps
if type xbps-install &>/dev/null; then
	alias Aa=remove_no_longer_needed_dependencies
	alias Af=find_package
	alias Ai=install_package
	alias Al=list_installed_packages
	alias Alog=output_log_file
	alias Ap=find_package_to_file
	alias Ar=remove_package
	alias As=show_package_details
	alias Au=update_packages

        alias check_all_packages_for_errors='xbps-pkgdb --all'
        alias downgrade_package='xdowngrade'
        alias find_file_in_available_packages='xlocate'
        alias find_package='xbps-query -R --search'
        alias find_package_to_file='xbps-query --ownedby'
        alias install_package='sudo xbps-install'
        alias install_package_as_dependency='xbps-pkgdb --mode auto'
        alias list_installation_sources='xbps-query --list-repos'
        alias list_installed_packages='xbps-query --list-pkgs'
        alias list_explicitly_installed_packages='xbps-query --list-manual-pkgs'
        alias list_files_of_package='xbps-query -R --files'
        alias list_manual_packages_by_individual_size='xbps-query --list-manual-pkg | sed "s#\(.*\)-.*#\1#g" | while read p ; do echo -n "$p " && LANG=en xbps-query --property installed_size --show $p ; done | sort --human-numeric-sort --key=2 | column --table'
        alias lock_package='xbps-pkgdb --mode hold'
        alias mark_package_explicitly_installed='xbps-pkgdb --mode manual'
        alias purge_cached_packages='xbps-remove --clean-cache'
        alias release_locked_package='xbps-pkgdb --mode unhold'
        alias remove_no_longer_needed_dependencies='sudo xbps-remove --remove-orphans'
        alias remove_package='sudo xbps-remove --recursive'
        alias show_link_to_changelog_for_package='xbps-query --property changelog'
        alias show_package_details='xbps-query -R --show'
        alias output_log_file='cat /var/log/socklog/xbps/current || echo "Log file not found."'
        alias update_packages='sudo xbps-install -S --update'
fi


# xmame
alias xmame='nice -15 xmame -ef 2 -s 3'
alias xmames4='nice -15 xmame.x11 -s 4'


# xrandr
if [ -n "$DISPLAY" ] ; then
        monitor=`xrandr --listactivemonitors | sed -n 's#^ \+.* \(.*\)#\1#p'`
        alias "xrandr_${MONITOR}_backlight_100_percent"="xrandr --output ${MONITOR} --brightness 1"
        alias "xrandr_${MONITOR}_backlight_120_percent"="xrandr --output ${MONITOR} --brightness 1.2"
        alias "xrandr_${MONITOR}_backlight_160_percent"="xrandr --output ${MONITOR} --brightness 1.6"
        alias "xrandr_${MONITOR}_backlight_200_percent"="xrandr --output ${MONITOR} --brightness 2"
        alias "xrandr_${MONITOR}_backlight_300_percent"="xrandr --output ${MONITOR} --brightness 3"
fi


# xset
alias disable_screensaver_and_dpms='xset s off && xset -dpms'


# yt-dlp
alias Ymp3='yt-dlp --extract-audio --audio-format mp3 --no-playlist'


# zfs
if type zfs &>/dev/null; then
        alias zfs_snapshots='zfs list -t snapshot'
        alias zfs_snapshot_names='zfs list -t snapshot | cut -f1'
fi


# zsh
debug_zsh_startup='zsh -ixc : 2>&1'
